��          �      �        C   	     M     ^     k  2   s     �     �  &   �     �     
  ;        J     M  ,   V  =   �  $   �     �  ;   �  ;   3  k   o     �     �  �    �   �  '   K     s     �  N   �  "   �  .   
  /   9  E   i  
   �  v   �     1	     8	  R   K	  w   �	  F   
  <   ]
     �
  =     �   X  =     1   @                           
                                    	                                         Adjust the width of the admin menu to accomodate longer menu items. Alternate Method Chris Dillon Current Deactivating this plugin will not delete anything. Default Find the %s stylesheet. For WordPress %s to %s, substitute %s. Let your admin menu breathe. New No - These settings will remain after deleting this plugin. OR Settings That stylesheet covers WordPress 4.0 and up. Then either copy its contents to your theme&apos;s stylesheet Then you can deactivate this plugin. Wider Admin Menu Yes - Deleting this plugin will also delete these settings. You do not have sufficient permissions to access this page. copy the file to your theme folder and add this to your theme&apos;s <code>functions.php</code> to load it: restore default width revert to the current width PO-Revision-Date: 2018-03-12 15:00+0200
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 2.0.3
Language: ru
Project-Id-Version: Plugins - Wider Admin Menu - Stable (latest release)
POT-Creation-Date: 
Last-Translator: 
Language-Team: 
 Отрегулируйте ширину меню администратора, чтобы разместить более длинные пункты меню. Альтернативный метод Крис Диллон Текущие Отключение этого плагина ничего не удалит. Текст по умолчанию Найдите таблицу стилей %s. Для WordPress %s к %s, замените %s. Пусть ваше меню администратора дышит. Новые Нет. Эти настройки будут сохранены после удаления этого плагина. Или Настройки Эта таблица стилей охватывает WordPress 4.0 и выше. Затем либо скопируйте его содержимое в таблицу стилей вашей темы Затем вы можете отключить этот плагин. Расширенное меню администратора Да. Удаление этого плагина также приведет к удалению этих параметров. У вас нет доступа к этой странице. скопируйте файл в папку темы и добавьте его в свою тему <code> functions.php </ code>, чтобы загрузить его: восстановить ширину по умолчанию вернуться к текущей ширине 