<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 *
 * @package winnbrain
 * @since 1.0
 * @version 1.0
 */

get_header();

global $text_domain;

$parents = get_post_ancestors($post->ID);
if(is_array($parents) && count($parents)>0){
    $args = array(
        'post_type' => 'page',
        'post__in' => $parents
    );
    $parents = get_posts($args);
    $parents = count($parents)>0?array_reverse($parents):[];
}

?>

<?php while ( have_posts() ) : the_post(); ?>

    <!--begin section-breadcrumbs-->
    <section class="section-breadcrumbs">
        <div class="breadcrumbs-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                            <?php
                            if(count($parents)){
                                foreach ($parents as $parent){
                                    echo '<li><a href="'.get_permalink($parent->ID).'">'.$parent->post_title.'</a></li>';
                                }
                            }
                            ?>
                            <li class="active"><?= get_the_title(); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-breadcrumbs-->

    <?php addSidebars($post->ID, 'top'); ?>

    <!--begin section-basic-content-->
    <section class="section-base section-basic-content light-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="section-title-level-1">
                        <h1><?= get_the_title(); ?></h1>
                    </div>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </section>
    <!--end section-basic-content-->

    <?php addSidebars($post->ID, 'bottom'); ?>

<?php endwhile; ?>

<?php get_footer();
