<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 * Template Name: Payment Page
 * Template Post Type: page
 *
 * @package winnbrain
 * @since 1.0
 * @version 1.0
 */

get_header();

global $text_domain;

$getTerm = @$_GET['term'];
$currTerm = '';

$terms = get_terms([
    'taxonomy' => 'category_payment',
    'hide_empty' => false,
]);
if(count($terms)>0){
    foreach ($terms as $item){
        if((int)$getTerm == (int)$item->term_id){
            $currTerm = $item;
            break;
        }
    }
}
if(!$currTerm){
    $currTerm = @$terms[0];
}


$parents = get_post_ancestors($post->ID);
if(is_array($parents) && count($parents)>0){
    $args = array(
        'post_type' => 'page',
        'post__in' => $parents
    );
    $parents = get_posts($args);
    $parents = count($parents)>0?array_reverse($parents):[];
}
$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'payment',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];
if(isset($currTerm->term_id)){
    $args['tax_query'] = [
        [
            'taxonomy' => 'category_payment',
            'field' => 'term_id',
            'terms' => $currTerm->term_id,
            'include_children' => false
        ]
    ];
}
$posts_array = get_posts($args);
$res = getPaged($posts_array);
$posts_array = $res['posts_array'];

?>

<?php while ( have_posts() ) : the_post(); ?>

<!--begin section-breadcrumbs-->
<section class="section-breadcrumbs">
    <div class="breadcrumbs-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                        <?php
                        if(count($parents)){
                            foreach ($parents as $parent){
                                echo '<li><a href="'.get_permalink($parent->ID).'">'.$parent->post_title.'</a></li>';
                            }
                        }
                        ?>
                        <li class="active"><?= get_the_title(); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-breadcrumbs-->

<?php addSidebars($post->ID, 'top'); ?>

<!--begin section-media-object-->
<section class="section-base section-media-object">
    <div class="container">
        <div class="section-title-level-1">
            <h1><?= get_the_title(); ?></h1>
        </div>
        <div class="row">

            <div class="col-xs-12">
                <div class="page-nav">
                    <?php if(count($terms)>0): ?>
                        <ul>
                            <?php
                            foreach ($terms as $item){
                                $active = '';
                                if($item->term_id == $currTerm->term_id){
                                    $active = ' class="active"';
                                }
                                echo '<li'.$active.'><a href="?term='.$item->term_id.'">'.$item->name.'</a></li>';
                            }
                            ?>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>

            <?php if(is_array($posts_array) && count($posts_array )>0): ?>
                <?php foreach($posts_array as $key => $post): ?>
                    <?php
                    $title = get_the_title($post->ID);
                    $detals = get_post_meta($post->ID, '_payment_detals_value_key', true);
                    ?>
                    <div class="col-xs-12">
                        <div class="media-obj-text">
                            <div class="row">
                                <div class="col-sm-3 col-xs-12">
                                    <div class="media-obj-title">
                                        <h3><?= strip_tags($title); ?></h3>
                                    </div>
                                    <div class="media-obj-subtitle">
                                        <?= wp_trim_words( strip_shortcodes(strip_tags($detals)), 20, '...' ); ?>
                                    </div>
                                </div>
                                <div class="col-sm-9 col-xs-12">
                                    <div class="media-obj-content bordered-bottom">
                                        <?= strip_shortcodes(strip_tags($post->post_content)); ?>
                                    </div>
                                    <div class="main-button-wrap">
                                        <button type="button" class="main-button gold-button" data-toggle="modal" data-target="#form-popup-1"><span><?= __('Order an invoice', $text_domain); ?></span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

            <?php getPaganation($res); ?>

        </div>
    </div>
</section>
<!--end section-media-object-->

<?php endwhile; ?>

<?php addSidebars($post->ID, 'bottom'); ?>

<?php get_footer(); ?>