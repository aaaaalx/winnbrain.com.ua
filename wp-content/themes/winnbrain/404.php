<?php
/**
 * The template for displaying 404 pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage winnbrain
 * @since 1.0
 * @version 1.0
 */

get_header();

global $text_domain;

?>
    <!--begin section-breadcrumbs-->
    <section class="section-breadcrumbs">
        <div class="breadcrumbs-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                            <li class="active"><?= __('Error 404', $text_domain); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-breadcrumbs-->

    <!--begin section-media-object-->
    <section class="section-base section-err text-center">
        <div class="section-title-level-1">
            <h1><?= __('Error 404', $text_domain); ?>: <span><?= __('Page not found', $text_domain); ?></span></h1>
        </div>
        <div class="container">
            <div class="row">
                <div class="divider"></div>
                <div class="col-md-6 col-md-offset-3 colsm-12 col-sm-offset-0">
                    <div class="inner-wrap">
                        <p><?= __('Perhaps you have typed the wrong address, or such a page on the site no longer exists', $text_domain); ?>.</p>
                    </div>
                    <a href="/" class="main-button gold-button"><span><?= __('Back to main', $text_domain); ?></span></a>
                </div>
            </div>
        </div>
    </section>
    <!--end section-media-object-->
<?php get_footer();