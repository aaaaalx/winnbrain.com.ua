<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage winnbrain
 */

get_header();

global $text_domain;
?>

    <!--begin section-breadcrumbs-->
    <section class="section-breadcrumbs">
        <div class="breadcrumbs-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                            <li class="active"><?= __('Searching results', $text_domain); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-breadcrumbs-->

    <!--begin section-media-object-->
    <section class="section-base section-search">
        <div class="container">
            <div class="section-title-level-1">
                <h1><?= __('Searching results', $text_domain); ?></h1>
            </div>
            <div class="section-description">
                <?php printf( __( 'Search Results for: %s', 'twentyfifteen' ), get_search_query() ); ?>
            </div>

            <?php if (have_posts()) : ?>
            <div class="row">
                <div class="col-xs-12">
                    <?php
                    // Start the loop.
                    while ( have_posts() ) {
                        the_post();
                        get_template_part( 'content', 'search' );
                    }
                    ?>
                </div>
            </div>

            <?php else: ?>
            <div class="section-description">
                <h4><?= __('Nothing was found by your request', $text_domain); ?>!</h4>
            </div>
            <?php endif; ?>

        </div>
    </section>
    <!--end section-media-object-->

<?php get_footer(); ?>
