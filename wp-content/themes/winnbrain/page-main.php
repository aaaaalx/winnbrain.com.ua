<?php
/**
* The template for displaying main page
*
* Template Name: Main Page
* Template Post Type: page
*
* @package winnbrain
* @since 1.0
* @version 1.0
*/

get_header(); ?>

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('main_content')): ?>
    <?php dynamic_sidebar('main_content') ?>
<?php endif; ?>

<?php
// Start the loop.
while ( have_posts() ) : the_post();

	/*
	 * Include the post format-specific template for the content. If you want to
	 * use this in a child theme, then include a file called called content-___.php
	 * (where ___ is the post format) and that will be used instead.
	 */
	 
	$content = get_the_content();
	if($content){
?>

        <!--begin section-basic-content-->
        <section class="section-base section-basic-content light-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?= $content; ?>
                    </div>
                </div>
            </div>
        </section>
        <!--end section-basic-content-->

	<?php
	}

// End the loop.
endwhile;
?>

<?php if(function_exists('dynamic_sidebar') && is_active_sidebar('main_content_bottom')): ?>
    <?php dynamic_sidebar('main_content_bottom') ?>
<?php endif; ?>

<?php get_footer(); ?>
