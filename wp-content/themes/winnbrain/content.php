<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage winnbrain
 */

global $text_domain;

$term = @wp_get_post_terms($post->ID, 'category')[0];
$parents = [];
if($term){
    $parents = get_category_parents($term->term_id, true, '[.]', false);
    if($parents){
        $parents = explode('[.]',$parents);
        $parents = array_filter($parents);
    }
}
?>
<!--begin section-breadcrumbs-->
<section class="section-breadcrumbs">
    <div class="breadcrumbs-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                        <?php
                        if(is_array($parents) && count($parents)){
                            foreach ($parents as $v){
                                echo '<li>'.$v.'</li>';
                            }
                        }
                        ?>
                        <li class="active"><?= get_the_title(); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-breadcrumbs-->

<?php addSidebars($post->ID, 'top'); ?>

<!--begin section-basic-content-->
<section class="section-base section-basic-content light-section">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="section-title-level-1">
                    <h1><?= get_the_title(); ?></h1>
                </div>
                <?php the_content(); ?>
            </div>
        </div>
    </div>
</section>
<!--end section-basic-content-->

<?php addSidebars($post->ID, 'bottom'); ?>

