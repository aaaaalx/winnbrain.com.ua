<?php
/**
* The template for displaying main page
*
* Template Name: Sendmail Page
* Template Post Type: page
*
* @package winnbrain
* @since 1.0
* @version 1.0
*/

get_header();

global $text_domain;
?>

<!--begin section-breadcrumbs-->
<section class="section-breadcrumbs">
    <div class="breadcrumbs-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-breadcrumbs-->

<?php
// Start the loop.
while ( have_posts() ) : the_post();

/*
 * Include the post format-specific template for the content. If you want to
 * use this in a child theme, then include a file called called content-___.php
 * (where ___ is the post format) and that will be used instead.
 */

$content = get_the_content();
?>
<!--begin section-media-object-->
<section class="section-base section-err text-center">
    <div class="section-title-level-1">
        <h1><?= get_the_title(); ?></h1>
    </div>
    <div class="container">
        <div class="row">
            <div class="divider"></div>
            <div class="col-md-6 col-md-offset-3 colsm-12 col-sm-offset-0">
                <div class="inner-wrap">
                    <?= $content; ?>
                </div>
                <a href="/" class="main-button gold-button"><span><?= __('Back to home', $text_domain); ?></span></a>
            </div>
        </div>
    </div>
</section>
<!--end section-media-object-->
<?php
// End the loop.
endwhile;
?>

<?php get_footer(); ?>
