<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage winnbrain
 * @since 1.0
 * @version 1.0
 */
$post_format = @get_queried_object()->has_archive;
$post_format = $post_format?'category_'.$post_format:'';
if(!$post_format){
    force_404();
}
get_header();
get_template_part( 'taxonomy', $post_format );
get_footer();