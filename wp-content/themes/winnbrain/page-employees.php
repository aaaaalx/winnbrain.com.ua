<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 * Template Name: Employees Page
 * Template Post Type: page
 *
 * @package winnbrain
 * @since 1.0
 * @version 1.0
 */

get_header();

global $text_domain;

$parents = get_post_ancestors($post->ID);
if(is_array($parents) && count($parents)>0){
    $args = array(
        'post_type' => 'page',
        'post__in' => $parents
    );
    $parents = get_posts($args);
    $parents = count($parents)>0?array_reverse($parents):[];
}
$args = [
    'posts_per_page'   => -1,
    'offset'           => 0,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    'meta_key'         => '',
    'meta_value'       => '',
    'post_type'        => 'employees',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'author'	   => '',
    'author_name'	   => '',
    'post_status'      => 'publish',
    'suppress_filters' => true
];
$posts_array = get_posts($args);
$res = getPaged($posts_array);
$posts_array = $res['posts_array'];
?>

<?php while ( have_posts() ) : the_post(); ?>

<!--begin section-breadcrumbs-->
<section class="section-breadcrumbs">
    <div class="breadcrumbs-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                        <?php
                        if(count($parents)){
                            foreach ($parents as $parent){
                                echo '<li><a href="'.get_permalink($parent->ID).'">'.$parent->post_title.'</a></li>';
                            }
                        }
                        ?>
                        <li class="active"><?= get_the_title(); ?></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end section-breadcrumbs-->

<?php addSidebars($post->ID, 'top'); ?>

<!--begin section-media-object-->
<section class="section-base section-media-object">
    <div class="section-title-level-1">
        <h1><?= get_the_title(); ?></h1>
    </div>

    <?php if(is_array($posts_array) && count($posts_array )>0): ?>
    <?php foreach($posts_array as $key => $post): ?>
    <?php
    $title = get_the_title($post->ID);
    $short_desc = get_post_meta($post->ID, '_employees_short_desc_value_key', true);
    $image = get_the_post_thumbnail_url($post->ID, 'block-image-medium');
    $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_228x305.jpg';
    ?>
    <div class="media-wrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="inner-wrap">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="img-box clickable">
                                    <img src="<?= $image; ?>" alt="">
                                </div>
                            </div>
                            <div class="col-sm-8 col-xs-12">
                                <div class="media-body-1 media-close">
                                    <div class="media-header">
                                        <h2><?= strip_tags($title); ?></h2>
                                    </div>
                                    <div class="media-decription">
                                        <?= strip_shortcodes(strip_tags($short_desc)); ?>
                                    </div>
                                    <div class="item-text bordered">
                                        <?= strip_shortcodes($post->post_content); ?>
                                    </div>
                                </div>
                                <button class="card-button closed"><span><?= __('Learn more', $text_domain); ?></span></button>
                                <div class="media-divider"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach; ?>
    <?php endif; ?>

    <?php getPaganation($res); ?>

</section>
<!--end section-media-object-->

<?php endwhile; ?>

<?php addSidebars($post->ID, 'bottom'); ?>

<?php get_footer(); ?>