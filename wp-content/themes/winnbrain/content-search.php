<?php
/**
 * The template part for displaying results in search pages
 *
 *
 * @package WordPress
 * @subpackage winnbrain
 */

?>

<div class="search-item-group">
    <div class="search-item-title">
        <h4><a href="<?= get_permalink($post->ID); ?>"><?php the_title(); ?></a></h4>
    </div>
    <div class="search-item-description">
        <?php the_excerpt(); ?>
    </div>
    <div class="search-item-link">
        <a href="<?= get_permalink($post->ID); ?>"><?= get_permalink($post->ID); ?></a>
    </div>
</div>
