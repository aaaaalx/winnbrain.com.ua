<?php
/*
 * @package winnbrain
 *
 * ================================
 * ADMIN PAGE
 * ================================
 *
 * */
function theme_add_admin_page(){
    global $text_domain;

    //add menu item
    add_menu_page(__('Theme Options', $text_domain), __('Theme', $text_domain), 'manage_options', 'the-theme', 'theme_options_page', 'dashicons-admin-generic', 110);

    //call custom admin page
    add_action('admin_init', 'theme_custom_settings');

}
add_action('admin_menu', 'theme_add_admin_page');

//custom admin page(settings)
function theme_custom_settings(){
    global $text_domain;

    //add field section
    add_settings_section('theme-options-section', __('General Theme Options', $text_domain), 'general_theme_options', 'the-theme');

    /*Google map key*/
    register_setting('general-setting-group', 'field_mapKey');
    add_settings_field('Map-key', __('Set Google map key', $text_domain), 'google_map_key', 'the-theme', 'theme-options-section');

    /*Institution list*/
    register_setting('general-setting-group', 'field_custom_sidebars');
    add_settings_field('Custom-sidebars', __('Add Custom Sidebars', $text_domain), 'custom_dynamic_sidebars', 'the-theme', 'theme-options-section');
}

//description for section
function general_theme_options(){

}


//template for settings options page
function theme_options_page(){
    require_once get_template_directory().'/inc/template/theme-admin.php';
}

//field output
function google_map_key(){
    global $text_domain;
    $value = get_option('field_mapKey');
    echo '<textarea class="regular-text code" rows="2" name="field_mapKey" id="field_mapKey">'.$value.'</textarea>';
    echo '<p>'.__('Enter google map key', $text_domain).'</p>';
}

//field output
function custom_dynamic_sidebars(){
    global $text_domain;
    $value = get_option('field_custom_sidebars');
    $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [type="text"/] [name="title"/] /></p>';
    $pattern .= '<p><textarea [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p><br>';
    insertDynamicField('field_custom_sidebars', __('Add block Field', $text_domain), $value, $pattern);
}


