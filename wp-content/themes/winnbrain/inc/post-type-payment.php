<?php
/*
 * @package winnbrain
 *
 * ================================
 * THEME CUSTOM POST TYPE - payment
 * ================================
 *
 * */

//custom post type
function custom_post_type_payment(){
    global $text_domain;

    $post_labels = [
        'name' => __( 'Payment', $text_domain ),
        'singular_name' => __( 'Payment', $text_domain ),
        'menu_name' => __( 'Payment', $text_domain ),
        'name_admin_bar' => __( 'Payment', $text_domain )
    ];
    $post_args = [
        'labels' => $post_labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => 'payment',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author', 'thumbnail'],
        'show_in_nav_menus' => true,
        'taxonomies' => ['category_payment'],
        'rewrite' => array( 'slug' => 'payment/%cat%', 'with_front' => true ),
        'map_meta_cap' => true,
    ];

    $cat_labels = array(
        'name'              => __( 'Payment Categories', $text_domain ),
        'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
        'search_items'      => __( 'Search Categories' ),
        'all_items'         => __( 'All Categories' ),
        'parent_item'       => __( 'Parent Category' ),
        'parent_item_colon' => __( 'Parent Category:' ),
        'edit_item'         => __( 'Edit Category' ),
        'update_item'       => __( 'Update Category' ),
        'add_new_item'      => __( 'Add New Category' ),
        'new_item_name'     => __( 'New Category Name' ),
        'menu_name'         => __( 'Categories' ),
    );
    $cat_args = array(
        'labels'            => $cat_labels,
        'hierarchical'      => true, // Set this to 'false' for non-hierarchical taxonomy (like tags)
        'public'            => true,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'publicly_queryable'=> true,
        'show_tagcloud'     => false,
        'rewrite'           => array( 'slug' => 'payment', 'hierarchical' => true )
    );

    register_taxonomy( 'category_payment', ['payment'], $cat_args );
    register_post_type('payment', $post_args);
}
add_action( 'init', 'custom_post_type_payment', 0 );

$thisCustomPosts['payment'] = true;

function payment_taxonomy_slug_rewrite_rules($wp_rewrite) {
    $rules['payment/(.+?)/_([^/]+)?$'] = 'index.php?category_payment=$matches[1]&post_type=payment&name=$matches[2]';
    $wp_rewrite->rules = $rules + $wp_rewrite->rules;
}
add_filter('generate_rewrite_rules', 'payment_taxonomy_slug_rewrite_rules');

//add meta boxes
function payment_add_meta_box(){
    global $text_domain;

    //detals
    add_meta_box('payment_detals', __('Detals', $text_domain), 'payment_detals_callback', 'payment', 'normal', 'high');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)

}

function payment_detals_callback($post){
    global $text_domain;

    //contacts
    wp_nonce_field('payment_save_data', 'payment_detals_meta_box_nonce');//add unique verifying field
    $short_desc = get_post_meta($post->ID, '_payment_short_desc_value_key', true);//get custom meta box
    $detals = get_post_meta($post->ID, '_payment_detals_value_key', true);//get custom meta box


    //detals
    echo '<p>'.__('Additional description', $text_domain).': </p>';
    echo '<p><textarea id="payment_detals_value_key" class="widefat datafield" title="'.__('Additional description', $text_domain).'" type="text" name="payment_detals_value_key">'.$detals.'</textarea></p>';

}

function payment_save_data($post_id){

    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    if(isset($_POST['payment_detals_meta_box_nonce']) &&
        wp_verify_nonce($_POST['payment_detals_meta_box_nonce'], 'payment_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }

    //detals
    if(isset($_POST['payment_detals_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['payment_detals_value_key'];
        update_post_meta($post_id, '_payment_detals_value_key', $my_data);//save data from meta box field
    }

}

add_action('add_meta_boxes', 'payment_add_meta_box');//add custom metabox
add_action('save_post', 'payment_save_data');//save custom meta box

/*
//categories additional settings

//set redirect to select category template
function payment_template_redirect( $template ) {
    $term = get_queried_object();
    if(isset($term->term_id)){
        $temp = get_term_meta( $term->term_id, 'category_payment_templates', true );
        if($temp){
            $currPath = substr($template, 0, strrpos($template, '/'));
            $template = $currPath.'/'.$temp;
        }
    }
    return $template;
}
add_filter( 'template_include', 'payment_template_redirect', 99 );

//taxonomies templates list
function get_category_payment_templates(){
    return [
        '' => '',
        'video' => 'taxonomy-category_payment-video.php',
        'images' => 'taxonomy-category_payment-images.php'
    ];
}

//display term meta fields on add
function category_payment_add_meta_fields( $taxonomy ) {
    //term settings
}
add_action( 'category_payment_add_form_fields', 'category_payment_add_meta_fields', 10, 2 );

//display term meta fields on edit
function category_payment_edit_meta_fields( $term, $taxonomy ) {
    $background = get_term_meta( $term->term_id, 'category_payment_background', true );
    $temp = get_term_meta( $term->term_id, 'category_payment_templates', true );

    //set header bg image
    $Id = 'category_payment_background';
    $Name = $Id;
    echo '<p><label for="' . $Id . '"><b>' . __('Background', 'winnbrain') . '</b></label>';
    echo '<div class="widget-block-img">';
    add_thickbox();
    if($background){
        $backgroundMime = get_file_mime_type($background);
        if(getFileFormat($backgroundMime) == 'video'){
            echo '<video width="150" height="150" controls preload="metadata">';
            echo '<source src="'.$background.'" type="'.$backgroundMime.'">';
            echo '</video>';
        }
        else{
            echo '<a class="thickbox" href="'.$background.'?TB_iframe=true&width=100%&height=100%"><img src="'.$background.'"></a>';
        }
    }
    echo '</div>';
    echo '<input data-id="'.$Id.'" data-options="multiple:false,type:image" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'winnbrain').'" />';
    echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
    echo '<input data-id="'.$Id.'" class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'winnbrain').'" />';
    echo '</p>';

    $temps = get_category_payment_templates();
    if(count($temps)>0){
        $Id = 'category_payment_templates';
        $Name = $Id;
        echo '<p><label for="'.$Id.'">' . __('Template', 'winnbrain') . ': </label>';
        echo '<select name="'.$Name.'" id="'.$Id.'"></p>';
        foreach ($temps as $k => $val) {
            echo '<option value="'.$val.'" '.selected($temp, $val, false).'>'.$k."</option>\n";
        }
    }

}
add_action( 'category_payment_edit_form_fields', 'category_payment_edit_meta_fields', 10, 2 );

// Save custom terms meta fields
function category_payment_save_taxonomy_meta( $term_id, $tag_id ) {

    //category_payment_background
    if (isset($_POST['category_payment_background'])) {
        update_term_meta($term_id, 'category_payment_background', $_POST['category_payment_background']);
    }

    //category_payment_templates
    if (isset($_POST['category_payment_templates'])) {
        update_term_meta($term_id, 'category_payment_templates', $_POST['category_payment_templates']);
    }

}
add_action( 'created_category_payment', 'category_payment_save_taxonomy_meta', 10, 2 );
add_action( 'edited_category_payment', 'category_payment_save_taxonomy_meta', 10, 2 );
*/