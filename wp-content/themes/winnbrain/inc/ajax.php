<?php
/*
 * @package winnbrain
 *
 *
 * */

//include ajax "contacts forms"
require_once get_template_directory().'/inc/ajax-contacts.php';

if(is_admin()){
    add_action('wp_ajax_nopriv_winnbrain_get_shortcode', 'get_shortcode');
    add_action('wp_ajax_winnbrain_get_shortcode', 'get_shortcode');
}
function get_shortcode(){
    $code = wp_strip_all_tags(@$_POST['data']);
    $code = str_replace('\"', '"', $code);
    echo do_shortcode($code);
    exit();
}

add_action('wp_ajax_nopriv_winnbrain_get_select_list', 'getSelectList');
add_action('wp_ajax_winnbrain_get_select_list', 'getSelectList');
function getSelectList(){
    $code = @$_POST['data'];
    if($code){
        $connected = @$code['connected'];
        $call = @$code['call'];
        $value = @$code['value'];
        if($connected && $call){
            $list = null;
            if($value){
                $list = call_user_func($call, $value);
            }
            elseif($value != null){
                $list = call_user_func($call);
            }
            if($list){
                $listJson = json_encode($list, true);
                echo '{"data":'.$listJson.', "connected":"'.$connected.'"}';
                exit();
            }
            else{
                echo '{"data":{}, "connected":"'.$connected.'"}';
                exit();
            }
        }
    }
}