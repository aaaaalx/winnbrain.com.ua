<?php
/*
 * @package winnbrain
 *
 *
 * */

add_action('wp_ajax_nopriv_winnbrain_save_contact_form', 'save_contact_form');
add_action('wp_ajax_winnbrain_save_contact_form', 'save_contact_form');

function save_contact_form(){
    global $text_domain;

    $_SESSION['contactFormMessage'] = '';

    $errorMess_1 = '<h1 style="color: red;">'.__('The message was not sent! Please try again.', $text_domain).'</h1>';
    $errorMess_2 = '<h1 style="color: red;">'.__('Fill all the fields, please!', $text_domain).'</h1>';
    $successMess = '<h1>'.__('Thank you!', $text_domain).'</h1>';
    $successMess .= '<p>'.__('We are grateful for your interest in our company', $text_domain).'</p>';
    $successMess .= '<p>'.__('Our specialists will contact you shortly', $text_domain).'</p>';

    $formTitle = wp_strip_all_tags(@$_POST['formTitle']);
    $formType = wp_strip_all_tags(@$_POST['formType']);

    if(!$formType){
        $_SESSION['contactFormMessage'] .= $errorMess_1;
        return true;
        exit();
    }

    $username = wp_strip_all_tags(@$_POST['username']);
    $phone = wp_strip_all_tags(@$_POST['phone']);
    $email = wp_strip_all_tags(@$_POST['email']);
    $company = wp_strip_all_tags(@$_POST['company']);
    $message = wp_strip_all_tags(@$_POST['message']);
    $details = wp_strip_all_tags(@$_POST['details']);

    if($formType == 'form-1' || $formType == 'form-2' || $formType == 'form-3'){
        if(!$username || !$phone){
            $_SESSION['contactFormMessage'] .= $errorMess_2;
            return true;
            exit();
        }
    }
    else{
        $_SESSION['contactFormMessage'] .= $errorMess_2;
        return true;
        exit();
    }

    $title = $username;
    $content = $formTitle?'<p>'.__('Form', $text_domain).': '.$formTitle.'</p>':'';
    $content .= $details?'<p>'.__('Details', $text_domain).': '.$details.'</p>':'';
    $content .= $title?'<p>'.__('Full name', $text_domain).': '.$title.'</p>':'';
    $content .= $phone?'<p>'.__('Phone', $text_domain).': '.$phone.'</p>':'';
    $content .= $email?'<p>'.__('Email', $text_domain).': '.$email.'</p>':'';
    $content .= $company?'<p>'.__('Company', $text_domain).': '.$company.'</p>':'';
    $content .= $message?'<p>'.__('Message', $text_domain).': '.$message.'</p>':'';

    //get admins emails
    $adminEmail = get_option('adminEmail');
    if($adminEmail && strpos($adminEmail, ',')>-1){
        $adminEmail = explode(',', $adminEmail);
        $adminEmail = array_filter($adminEmail);
    }

    $subjectText = __('Winnbrain Contact Form', $text_domain);

    $ards = [
        'post_title' => $title,
        'post_content' => $content,
        'post_author' => 1,
        'post_status' => 'publish',
        'post_type' => 'contacts'
    ];

    $postId = wp_insert_post($ards);

    if($postId !== 0){
        //send to admin
        $from = 'support@'.$_SERVER['SERVER_NAME'];
        $subject = $subjectText;
        if(!is_array($adminEmail)){
            $to = trim($adminEmail);
            $headers[] = 'From: '.get_bloginfo('name').' <'.$from.'>'. "\r\n";
            $headers[] = 'content-type: text/html';
            if(!wp_mail($to, $subject, $content, $headers)){
                $_SESSION['contactFormMessage'] .= $errorMess_1;
            }
        }
        elseif(count($adminEmail)>0){
            foreach($adminEmail as $m){
                $to = trim($m);
                $headers[] = 'From: '.get_bloginfo('name').' <'.$from.'>'. "\r\n";
                $headers[] = 'content-type: text/html';
                wp_mail($to, $subject, $content, $headers);
            }
        }
        $_SESSION['contactFormMessage'] .= $successMess;
        return true;
        exit();
    }
    else{
        $_SESSION['contactFormMessage'] .= $errorMess_1;
        return true;
        exit();
    }

}