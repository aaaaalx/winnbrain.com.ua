<?php
/*
 * @package winnbrain-land
 *
 * ================================
 * ADMIN PAGE
 * ================================
 *
 * */

function admin_load_admin_scripts($hook){

    //link choosing support (look js callWpLinkPicker func)
    wp_enqueue_script( 'wp-link' );

    wp_enqueue_style('thickbox');
    wp_enqueue_script('thickbox');

    wp_register_style('admin_main', get_template_directory_uri().'/css/main.admin.css', [], '1.0', 'all');
    wp_enqueue_style('admin_main');

    //enable media files
    wp_enqueue_media();
    //enable visual editor
    wp_enqueue_editor();

    //enable wp color picker
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'underscore' );

    wp_register_script('admin_main_script', get_template_directory_uri().'/js/main.admin.js', ['jquery'], '1.0', true);
    wp_enqueue_script('admin_main_script');

    //dropdown icons list
    wp_register_style('admin_dropdown', get_template_directory_uri().'/libs/ddslick-master/css/ddslick.css', [], '1.0', 'all');
    wp_enqueue_style('admin_dropdown');
    wp_register_script('admin_dropdown_script', get_template_directory_uri().'/libs/ddslick-master/js/jquery.ddslick.min.js', ['jquery'], '1.0', true);
    wp_enqueue_script('admin_dropdown_script');

}
add_action('admin_enqueue_scripts', 'admin_load_admin_scripts');

function user_load_scripts($hook){

    //enable media files
    wp_enqueue_style('wp-mediaelement');
    wp_enqueue_script('wp-mediaelement');

    //css styles

    wp_register_style('bootstrap', get_template_directory_uri().'/libs/css/bootstrap.css', [], '1.0', 'all');
    wp_enqueue_style('bootstrap');

    wp_register_style('slick', get_template_directory_uri().'/libs/css/slick.css', [], '1.0', 'all');
    wp_enqueue_style('slick');

    wp_register_style('owl-carousel', get_template_directory_uri().'/libs/css/owl.carousel.min.css', [], '1.0', 'all');
    wp_enqueue_style('owl-carousel');

    wp_register_style('style', get_template_directory_uri().'/css/style.css', [], '1.0', 'all');
    wp_enqueue_style('style');

    wp_register_style('style_custom', get_template_directory_uri().'/css/style-custom.css', [], '1.0', 'all');
    wp_enqueue_style('style_custom');

    //wp_register_style('user_map', '//maps.googleapis.com', [], '1.0', 'all');
    //wp_enqueue_style('user_map');

    //wp_register_style('user_googleadservices', '//www.googleadservices.com', [], '1.0', 'all');
    //wp_enqueue_style('user_googleadservices');

    //set rel as "dns-prefetch" for googleadservices.com & maps.googleapis.com
    add_filter('style_loader_tag', 'user_map_style_loader_tag');
    function user_map_style_loader_tag($tag){
        if(strpos($tag, 'user_googleadservices') || strpos($tag, 'user_map')){
            $tag = str_replace('stylesheet', 'dns-prefetch', $tag);
        }
        return $tag;
    }

    //js scripts
    wp_register_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', '', '', true);
    wp_enqueue_script('jquery');

    wp_register_script('bootstrap', get_template_directory_uri().'/libs/js/bootstrap.js', '', '', true);
    wp_enqueue_script('bootstrap');

    wp_register_script('slick', get_template_directory_uri().'/libs/js/slick.js', '', '', true);
    wp_enqueue_script('slick');

    wp_register_script('owl_carousel', get_template_directory_uri().'/libs/js/owl.carousel.js', '', '', true);
    wp_enqueue_script('owl_carousel');

    wp_register_script('maskedinput', get_template_directory_uri().'/libs/js/jquery.maskedinput.min.js', '', '', true);
    wp_enqueue_script('maskedinput');

    wp_register_script('main', get_template_directory_uri().'/js/main.js', '', '', true);
    wp_enqueue_script('main');

    wp_register_script('main_custom', get_template_directory_uri().'/js/main-custom.js', '', '', true);
    wp_enqueue_script('main_custom');

}
add_action('wp_enqueue_scripts', 'user_load_scripts');