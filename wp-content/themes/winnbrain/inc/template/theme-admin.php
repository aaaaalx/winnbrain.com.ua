<?php global $text_domain; ?>
<h1><?= __('Theme Options', $text_domain); ?></h1>
<?php settings_errors(); ?>
<form method="post" action="options.php" class="theme-general-form">
    <?php add_thickbox(); ?>
    <?php settings_fields('general-setting-group'); ?>
    <?php do_settings_sections('the-theme'); ?>
    <?php submit_button(__('Save changes', $text_domain), 'primary', 'btnSubmit'); ?>
</form>
