<?php
/*
 * @package winnbrain
 *
 * ================================
 * THEME CUSTOM POST TYPE CONTACTS
 * ================================
 *
 * */


add_action('init', 'contact_custom_post_type');//initialize(add) custom post type
add_filter('manage_contacts_posts_columns', 'contact_set_columns');//"manage_posts_columns" as first argument - to editing default posts list!
add_action('manage_contacts_posts_custom_column', 'contact_custom_column', 10, 2);//set custom description for columns. 10 - default sequence position. 2 - args count

add_action('add_meta_boxes', 'contact_add_meta_box');//add custom metabox

//add administrators's contact emails field to theme options
//call custom admin page
add_action('admin_init', 'theme_contacts_emails');
//custom admin page(settings)
function theme_contacts_emails(){
    global $text_domain;

    /*Admin contact emails*/
    register_setting('general-setting-group', 'adminEmail');
    add_settings_field('Contacts-emails', __('Administrators\' contact emails', $text_domain), 'contact_emails_field', 'the-theme', 'theme-options-section');
}
//field output
function contact_emails_field(){
    global $text_domain;

    $value = get_option('adminEmail');
    echo '<textarea class="regular-text code" rows="6" name="adminEmail" id="adminEmail">'.esc_attr($value).'</textarea>';
    echo '<p>'.__('Enter email addresses separated by commas.', $text_domain).'</p>';
}

//contact cpt(custom post type)
function contact_custom_post_type(){
    global $text_domain;

    $labels = [
        'name' => __('Messages', $text_domain),
        'singular_name' => __('Message', $text_domain),
        'menu_name' => __('Messages', $text_domain),
        'name_admin_bar' => __('Message', $text_domain)
    ];
    $args = [
        'labels' => $labels,
        'show_ui' => true,
        'show_in_menu' => true,
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 26,
        'menu_icon' => 'dashicons-email-alt',
        'supports' => ['title', 'editor', 'author'],
        'show_in_nav_menus' => true,
        'public' => false,
        'publicly_queryable' => false,
        'query_var' => false,
    ];
    register_post_type('contacts', $args);
}

function contact_set_columns($columns){//setup custom columns
    global $text_domain;

    $newColumns = [];
    $newColumns['cb'] = '<input type="checkbox" />';
    $newColumns['title'] = __('Full name', $text_domain);
    $newColumns['email_status'] = __('Status', $text_domain);
    $newColumns['condition'] = __('Condition', $text_domain);
    $newColumns['message'] = __('Message', $text_domain);
    $newColumns['date'] = __('Date');
    return $newColumns;
}
function contact_custom_column($column, $post_id){//setup custom columns descriptions
    global $text_domain;

    switch($column){
        case 'message':
            echo get_the_excerpt();
            break;
        case 'email_status':
            $value = get_post_meta($post_id, '_contact_email_value_key', true);//get custom meta box
            if(!$value){
                echo __('New', $text_domain);
            }
            else{
                echo __('Opened', $text_domain);
            }
            break;
        case 'condition':
            $value = (int)get_post_meta($post_id, '_contact_condition_value_key', true);//get custom meta box
            $value = !$value?1:$value;
            if($value === 1){
                echo __('Not processed', $text_domain);
            }
            elseif($value === 2){
                echo __('In processing', $text_domain);
            }
            else{
                echo __('Processed', $text_domain);
            }
    }
}

/*Contact meta boxes*/
function contact_add_meta_box(){
    global $text_domain;

    add_meta_box('contact_email_status', __('Status', $text_domain), 'contacts_callback', 'contacts', 'side', 'default');//wp prebuilt metal box adding function. "side" - position(normal|side|advanced). "default" - priority(high|default|low)
}

function contacts_callback($post){
    global $text_domain;

    wp_nonce_field('contacts_save_data', 'contacts_meta_box_nonce');//add unique verifying field
    $value = get_post_meta($post->ID, '_contact_email_value_key', true);//get custom meta box
    $condition = get_post_meta($post->ID, '_contact_condition_value_key', true);//get custom meta box
    $condition = !$condition?1:$condition;

    //contact_email_value_key
    echo '<p>'.__('Email status', $text_domain).': ';
    if(!$value){
        echo '<lable for="winnbrain_contact_email_value_key">'.__('New', $text_domain).'</lable>';
    }
    else{
        echo '<lable for="winnbrain_contact_email_value_key">'.__('Opened', $text_domain).'</lable>';
    }
    update_post_meta($post->ID, '_contact_email_value_key', 1);//set "1" as field's value to indicate whether email was readed
    echo '</p><hr>';

    //contact_condition_value_key
    echo '<p>'.__('Condition', $text_domain).':</p>';
    echo '<p>'.__('Not processed', $text_domain).': <input type="radio" value="1" name="contact_condition_value_key" '.isChecked(1, $condition).'></p>';
    echo '<p>'.__('In processing', $text_domain).': <input type="radio" value="2" name="contact_condition_value_key" '.isChecked(2, $condition).'></p>';
    echo '<p>'.__('Processed', $text_domain).': <input type="radio" value="3" name="contact_condition_value_key" '.isChecked(3, $condition).'></p>';
    update_post_meta($post->ID, '_contact_condition_value_key', $condition);
}
//test if option is checked
function isChecked($val, $opt){
    if((int)$val === (int)$opt){
        return 'checked';
    }
}

function contacts_save_data($post_id){
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }

    //condition
    if(isset($_POST['contacts_meta_box_nonce']) &&
        wp_verify_nonce($_POST['contacts_meta_box_nonce'], 'contact_add_meta_box')){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['contact_condition_value_key'])){//check is meta box verifying exist
        $my_data = $_POST['contact_condition_value_key'];
        update_post_meta($post_id, '_contact_condition_value_key', $my_data);//save data from meta box field
    }
}
add_action('save_post', 'contacts_save_data');//save custom meta box

//display count of new emails
add_action( 'admin_menu', 'add_user_menu_bubble' );
function add_user_menu_bubble(){
    global $menu;
    $posts = get_posts([
        'post_type' => 'contacts',
        'post_status' => 'publish',
        'numberposts' => -1
        // 'order'    => 'ASC'
    ]);
    $nPostsCount = 0;
    if(count($posts)>0){
        foreach($posts as $post){
            $value = get_post_meta($post->ID, '_contact_email_value_key', true);
            if(!$value){
                $nPostsCount += 1;
            }
        }
    }
    if($nPostsCount>0){
        foreach($menu as $key => $value){
            if($value[2] == 'edit.php?post_type=contacts'){
                $menu[$key][0] .= ' <span class="update-plugins count-'.$nPostsCount.'"><span class="plugin-count">'.$nPostsCount.'</span></span>';
                return;
            }
        }
    }
}