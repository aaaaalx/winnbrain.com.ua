$(document).ready(function(){
    /*registr form submit*/
    var formProcessing = false;
    $('.form').on('submit', function(e){
        e.preventDefault();
        if(formProcessing){
            return;
        }
        formProcessing = true;
        var form = $(this);

        var $formTitle = form.find('.hidden-data');
        var $formType = '';
        if($formTitle.length){
            $formType = $formTitle.data('form');
        }

        if(!$formType){
            return;
        }

        var date = {};

        if($formType == 'form-1' || $formType == 'form-2'){
            date.username = $(form).find('.username').val();
            date.phone = $(form).find('.phone').val();
            date.email = $(form).find('.email').val();
            date.details = $(form).find('.input-rate').val();
        }
        else if($formType == 'form-3'){
            date.username = $(form).find('.username').val();
            date.company = $(form).find('.company').val();
            date.phone = $(form).find('.phone').val();
            date.email = $(form).find('.email').val();
            date.message = $(form).find('.message').val();
        }
        else{
            return false;
        }

        date.action = 'winnbrain_save_contact_form';
        date.formTitle = $formTitle.val();
        date.formType = $formType;

        var url = form.data('url');
        form.append('<div class="form-cover"></div>');
        $.ajax({
            url: url,
            type: 'post',
            data: date,
            error: function(response){},
            success: function(response){
                if(response){
                    formProcessing = false;
                    form.find('.form-cover').remove();
                    window.location = '/alert'
                }
            }
        });

    });

    /*scroll-navigation*/
    $('.scroll').on('click', function(e) {
        var section = $(this).attr('href').substr($(this).attr('href').indexOf('#'));
        var $section = $(section);
        if($section.length){
            $('html, body').animate({
                scrollTop: $section.offset().top-90
            }, 1000);
            $(".menu-wrap").removeClass("show");
            return false;
        }
        else {
            return true;
        }
    });

    //if select is changed, inserting requested value(object list) into targeted(data-target) select by passed url(data-url)
    $('.aimedSelectList').on('change', function(e) {
        var url =  $(this).data('url');
        var val = $(this).val();
        var owner = $(this).parents('.owner');
        if(owner.length){
            owner = owner.data('owner');
        }
        var target = $(this).data('target');
        if(url && val && target){
            getDataByAjax(url, target, val, owner);
        }
    });

    //set same height for elements (.same-height) united by groups (data-group="")
    function setElementsGroupsHeight(actFromWinWidth){
        var elems = $('.same-height');
        if(actFromWinWidth && window.innerWidth < actFromWinWidth){
            elems.css('height', '');
            return false;
        }
        var sameHeightArr = {};
        if(elems.length){
            elems.each(function (k,v) {
                var elem = $(v);
                var data = elem.data('group');
                if(data){
                    if(!sameHeightArr[data]){
                        sameHeightArr[data] = {};
                    }
                    if(!sameHeightArr[data].height || sameHeightArr[data].height<elem.height()){
                        sameHeightArr[data].height = elem.height();
                    }
                    sameHeightArr[data].elems = sameHeightArr[data].elems?sameHeightArr[data].elems:[];
                    sameHeightArr[data].elems.push(elem);
                }
            });
            if(Object.keys(sameHeightArr).length){
                $.each(sameHeightArr, function (k,v) {
                    $.each(v.elems, function (k2,v2) {
                        $(v2).css('height', v.height+'px');
                    })
                });
            }
        }
    }
    setElementsGroupsHeight(993);
    $(window).resize(function(){
        setElementsGroupsHeight(993);
    });

    /*rate choice form popup tarif.html*/
    function getRate(){
        var rateId = $(".table-lg thead span");
        var parent = rateId.parents('.section-base');
        var title = parent.find('.tableTitle').text();
        var arr = [];
        var myArray = {};
        var rateChoice = $(".rate-choice");
        var rate = $(".form-1 .input-rate");
        rateId.each(function(index, element){
            myArray[$(element).attr('id')] = $(element).text()+' ('+title+')';
        });
        for (var i in myArray){
            arr.push(myArray[i]);
        }
        rateChoice.click(function(){
            var index = $(this).parent().index()-1;
            $(this).val(arr[index]);
            rate.val("Выбранный тариф:" + " " + $(this).val());
        });
    }
    getRate();

});