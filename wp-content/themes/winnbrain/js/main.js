var windowWidth = window.innerWidth;
$(document).ready(function(){/*slider-intro*/
	
	$('.slider-intro').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		nextArrow: '<div class="arrow-next"></div>',
		prevArrow: '<div class="arrow-prev"></div>',
		responsive: [
		{
			breakpoint: 768,
			settings: {
				dots: false,
			}
		}
		]

	});
	$(".slider-4-items").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 4000,
		responsive: [
		{
			breakpoint: 500,
			settings: {
				slidesToShow: 1,
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
			}
		},
		{
			breakpoint: 992,
			settings: {
				slidesToShow: 3,
			}
		}
		]
	});	

	$(".slider-1-item").slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoheight: true,
		autoplaySpeed: 4000,
		dots: true,
		nextArrow: '<div class="arrow-next"></div>',
		prevArrow: '<div class="arrow-prev"></div>',
		responsive: [
		{
			breakpoint: 768,
			settings: {
				arrows: false,
			}
		}
		]
	});

	/*responsive slider index.html section-list-item-1*/
	function respSlider(){
		var respSlider = $("#sli-slider .row");
		var windowWidth = window.innerWidth;
		if (windowWidth <= 992){
			respSlider.addClass("resp-slider");
		}	

	}
	$(window).resize(function(){
		respSlider();
	});
	respSlider();


	$(".resp-slider").slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		nextArrow: '<div class="arrow-next"></div>',
		prevArrow: '<div class="arrow-prev"></div>',
		autoplaySpeed: 4000,
		responsive: [
		{
			breakpoint: 650,
			settings: {
				slidesToShow: 1,
			}
		},
		{
			breakpoint: 768,
			settings: {
				slidesToShow: 2,
			}
		},		
		]
	});

	


	/*responsive menu*/
	$(".menu-collapse-button").click(function(){
		$(".nav-panel").toggleClass("show");
	});

	/*footer add button social-toggle*/
	function socialToggleBtn(){
		var itemLength = $(".social-buttons-list .social-button-box").length;
		if (itemLength > 3){
			$('<button class="social-toggle"></button>').appendTo(".social-buttons-list");
		}
		else {
			$(".social-toggle").remove();
		}
	};
	socialToggleBtn();

	/*footer social links*/
	$(".social-toggle").click(function(){
		$(this).parent().toggleClass("closed").toggleClass("open");
	});
});

/*header scroll animations*/
var scrolled = 100;
var topHeader = $(".top-header-wrap");
var preButton = $(".preheader-button");
var windowWidth = window.innerWidth;
function headerScroll(){
	$(window).scroll(function(){ 
		if ($(window).scrollTop() > scrolled && windowWidth > 768){
			topHeader.slideUp();
			preButton.show();
		}
		else {
			topHeader.slideDown();
			preButton.hide();
		}
	});
}
preButton.click(function(){
	topHeader.slideToggle();
});
headerScroll();

/*header search-form*/
function searchAnimate(){			
	var input = $(".search-input");
	var menu 	= $(".menu-list");
	var windowWidth = window.innerWidth;
	if (windowWidth <= 992) {
		input.focus(function(){	
			menu.show();			
		});
	}
	else {
		if (input.is(":focus")){
			menu.hide();
		}	
		input.focus(function(){
			menu.hide();
		});
		input.blur(function(){
			menu.show();
		});			
	}

}
searchAnimate();
$(window).resize(searchAnimate);

/*header top-header wrap appendTo bottom-header-wrap if window.width <=320px*/
function topHeaderResponsive(){
	var windowWidth = window.innerWidth;
	var topHeader = $(".top-header-wrap");
	var navPanel = $(".nav-panel");
	if (windowWidth < 320){
		topHeader.prependTo(navPanel);
	}
};
$(window).resize(function(){
	topHeaderResponsive();
});
topHeaderResponsive();


/*Media object title autoheight function*/
function MediaAutoheight(){
	var minHeight = 0;	
	var windowWidth = window.innerWidth;
	var mediaTitle = $(".media-title");
	if (windowWidth >= 993){
		mediaTitle.children().each(function(){
			var height = parseInt($(this).height());
			if (height > minHeight){
				minHeight = height;
			}
			mediaTitle.height(minHeight);
		});
	} 
	if (windowWidth <= 992){
		mediaTitle.css("height", "auto");
	}
};
$(window).resize(function(){
	MediaAutoheight();
});
MediaAutoheight();

/*Media object text autoheight function*/
function MediaTextAutoheight(){
	var minHeight = 0;	
	var windowWidth = window.innerWidth;
	var mediaText = $(".media-text");
	if (windowWidth >= 993){
		mediaText.children().each(function(){
			var height = parseInt($(this).height());
			if (height > minHeight){
				minHeight = height;
			}
			mediaText.height(minHeight);
		});
	} 
	if (windowWidth <= 992){
		mediaText.css("height", "auto");
	}
};
MediaTextAutoheight();
$(window).resize(function(){
	MediaTextAutoheight();
});

/*popup testimonials index.html*/
function popupSliderOneItem() {
	var windowWidth = window.innerWidth;
	if (windowWidth > 768) {
		$(document).on('click', '.media-presentation .item', function () {
			imagen = $(this).find(".img-box img").attr("src");
			$(".modal-body").html("<img src=" + imagen + ">");
			$('#modal-1-item-slider').modal('show');
		});
	}
	else {
		$(document).off('click', '.media-presentation  .item');
	}

}

popupSliderOneItem();
$(window).resize(function(){
	popupSliderOneItem();
});

/*popup testimonials testimonials.html*/
function popupTestimonials() {
	var windowWidth = window.innerWidth;
	if (windowWidth > 768) {
		$(document).on('click', '.img-box.clickable', function () {
			imagen = $(this).find("img").attr("src");
			$(".modal-body").html("<img src=" + imagen + ">");
			$('#modal-testimonials').modal('show');
		});
	}
	else {
		$(document).off('click', '.img-box.clickable');
	}

}

popupTestimonials();
$(window).resize(function(){
	popupTestimonials();
});


function showMoreMedia(){
	var button = $(".employess-page .card-button");
	button.click(function(){
		$(this).toggleClass("closed").toggleClass("open");
		$(this).prev().toggleClass("media-close").toggleClass("media-show");
	})
}

showMoreMedia();

/*phone enter mask*/
$(function(){  
	$("#input-phone-1, #input-phone-2, #input-phone-3").mask("+38 (999) 999-99-99");
});

/*scroll-navigation*/
$('.scroll').on('click', function(e) {
	e.preventDefault();
	var section = $(this).attr('href').substr($(this).attr('href').indexOf('#'));
	var $section = $(section);

	$('html, body').animate({
		scrollTop: $section.offset().top-90
	}, 1000);
	$(".menu-wrap").removeClass("show");
});


/*input number custom css*/
(function() {
	
	window.inputNumber = function(el) {

		var min = el.attr('min') || false;
		var max = el.attr('max') || false;

		var els = {};

		els.dec = el.prev();
		els.inc = el.next();

		el.each(function() {
			init($(this));
		});

		function init(el) {

			els.dec.on('click', decrement);
			els.inc.on('click', increment);

			function decrement() {
				var value = el[0].value;
				value--;
				if(!min || value >= min) {
					el[0].value = value;
				}
			}

			function increment() {
				var value = el[0].value;
				value++;
				if(!max || value <= max) {
					el[0].value = value++;
				}
			}
		}
	}
})();

inputNumber($('.item-counter'));

/*total-price-label scroll (calculator)*/

$(window).scroll(function() {
	var labelScroll = $("#label-scroll")
	var topScroll = $(document).scrollTop();
	labelScroll.css('top', (topScroll * 0.65)); 
});
