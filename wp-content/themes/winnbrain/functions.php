<?php
if (!session_id()) {
    session_start();
}

$text_domain = 'winnbrain';

//inqlude enqueue
require get_template_directory().'/inc/enqueue.php';
//include ajax
require_once get_template_directory().'/inc/ajax.php';
//include admin customization
require get_template_directory().'/inc/function-admin.php';

//inqlude custom post types
require get_template_directory().'/inc/post-type-contacts.php';
require get_template_directory().'/inc/post-type-testimonials.php';
require get_template_directory().'/inc/post-type-employees.php';
require get_template_directory().'/inc/post-type-payment.php';

//mime types
require_once get_template_directory().'/inc/mime_type_lib.php';

/*WIDGETS*/

//include widget "Main intro"
require_once get_template_directory().'/widgets/widget-intro.php';
//include widget "Info"
require_once get_template_directory().'/widgets/widget-info.php';
//include widget "Faq"
require_once get_template_directory().'/widgets/widget-faq.php';
//include widget "Services"
require_once get_template_directory().'/widgets/widget-services.php';
//include widget "Form"
require_once get_template_directory().'/widgets/widget-form.php';
//include widget "Advantages"
require_once get_template_directory().'/widgets/widget-advantages.php';
//include widget "Features"
require_once get_template_directory().'/widgets/widget-features.php';
//include widget "Main partners"
require_once get_template_directory().'/widgets/widget-partners.php';
//include widget "Main offer"
require_once get_template_directory().'/widgets/widget-offer.php';
//include widget "Main testimonials"
require_once get_template_directory().'/widgets/widget-testimonials.php';
//include widget "Main testimonials 2"
require_once get_template_directory().'/widgets/widget-testimonials-2.php';
//include widget "Main contacts"
require_once get_template_directory().'/widgets/widget-contacts.php';
//include widget "Form 2"
require_once get_template_directory().'/widgets/widget-form-2.php';
//include widget "Social links"
require_once get_template_directory().'/widgets/widget-soc-links.php';
//include widget "Widget Scripts"
require_once get_template_directory().'/widgets/widget-scripts.php';
//include widget "Widget Tariffs"
require_once get_template_directory().'/widgets/widget-tariffs.php';
//include widget "Widget Pages"
require_once get_template_directory().'/widgets/widget-pages.php';

//hide admin bar
show_admin_bar(false);

function wp_custom_new_menu() {
    global $text_domain;

    register_nav_menus(
        [
            'main_menu' => __( 'Main menu' ),
            'bottom_menu' => __( 'Bottom menu', $text_domain )
        ]
    );
}
add_action( 'init', 'wp_custom_new_menu' );

//enable shortcodes in the text widgets
add_filter('widget_text','do_shortcode');

//do shortcode
add_filter('the_content', 'do_shortcode', 11);

//turn on thumbnails support in posts
add_theme_support('post-thumbnails');

//add menus support
add_theme_support( 'menus' );

//move theme localization into the theme folder
add_action( 'after_setup_theme', 'localization_dir_setup' );
function localization_dir_setup(){
    global $text_domain;

    load_theme_textdomain($text_domain, get_template_directory().'/languages');
}

//add icons to admin panel
add_action('admin_head', 'show_favicon');
function show_favicon() {
    echo '<link rel="shortcut icon" href="'.get_stylesheet_directory_uri().'/img/icons/Winnbrain_favicon_32x32_v1-1.ico" type="image/x-icon" sizes="32x32">';
    echo '<link rel="shortcut icon" href="'.get_stylesheet_directory_uri().'/img/icons/Winnbrain_favicon_64x64_v1-1.ico" type="image/x-icon" sizes="64x64">';
}

//remove items from admin main menu
function remove_menus(){
  //remove_menu_page( 'index.php' );                  //Dashboard
  //remove_menu_page( 'jetpack' );                    //Jetpack* 
  //remove_menu_page( 'edit.php' );                   //Posts
  //remove_menu_page( 'upload.php' );                 //Media
  //remove_menu_page( 'edit.php?post_type=page' );    //Pages
  remove_menu_page( 'edit-comments.php' );          //Comments
  //remove_menu_page( 'themes.php' );                 //Appearance
  //remove_menu_page( 'plugins.php' );                //Plugins
  //remove_menu_page( 'users.php' );                  //Users
  //remove_menu_page( 'tools.php' );                  //Tools
  //remove_menu_page( 'options-general.php' );        //Settings
}
add_action( 'admin_menu', 'remove_menus' );

//don't show console input: "JQMIGRATE: Migrate is installed, version 1.4.1"
add_action( 'wp_default_scripts', function( $scripts ) {
    if ( ! empty( $scripts->registered['jquery'] ) ) {
        $scripts->registered['jquery']->deps = array_diff( $scripts->registered['jquery']->deps, array( 'jquery-migrate' ) );
    }
} );

//get attachment by its url
function get_attachment_file_id($url) {
    global $wpdb;
    $pUrl = parse_url($url);
    $url = @$pUrl['path'];
    $url = str_replace('/wp-content/uploads/', '', $url);
    $attachment = $wpdb->get_col($wpdb->prepare("SELECT post_id FROM $wpdb->postmeta WHERE meta_value='%s';", $url));
    return $attachment[0];
}

//return array of widgets instance with their fields
function getWidgetFieldByClassName($className){
    if(class_exists($className)){
        $widget = new $className;
        $id_base = $widget->id_base;
        $settings = $widget->get_settings();
        $res = [];
        if(count($settings)>0){
            foreach($settings as $k => $v){
                $res[$id_base.'-'.$k] = $v;
            }
        }
        return $res;
    }
    return false;
}

//get only format from mime
function getFileFormat($mime){
    if($mime){
        $mime = substr($mime, 0, strpos($mime, '/'));
        return $mime;
    }
    return false;
}

function winnbrain_land_widgets_init() {
    global $text_domain;

    register_sidebar([
        'name'          => __( 'Tag Head', $text_domain ),
        'id'            => 'head',
        'description'   => __( '<head>', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
	register_sidebar([
        'name'          => __( 'Tag Body', $text_domain ),
        'id'            => 'body',
        'description'   => __( '<body>', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Header: Emails', $text_domain ),
        'id'            => 'header_emails',
        'description'   => __( 'Header', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Header: Phones', $text_domain ),
        'id'            => 'header_phones',
        'description'   => __( 'Header', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Main Content', $text_domain ),
        'id'            => 'main_content',
        'description'   => __( 'Main body content', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Copyright', $text_domain ),
        'id'            => 'copyright',
        'description'   => __( 'Copyright', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);
    register_sidebar([
        'name'          => __( 'Footer: Social Links', $text_domain ),
        'id'            => 'footer_soc_links',
        'description'   => __( 'Social Links', $text_domain ),
        'before_widget' => '',
        'after_widget'  => '',
        'before_title'  => '',
        'after_title'   => '',
    ]);

}
add_action('widgets_init', 'winnbrain_land_widgets_init');

function force_404() {
    global $wp_query; //$posts (if required)
    status_header( 404 );
    nocache_headers();
    include( get_query_template( '404' ) );
    die();
}

//dynamic fields
//pattern template: <p><input [title="My Title"/] [type="text"/] [name=""/] [class="field"/] [data="type:image"/] [value=""/] /></p>
//pattern fields types: input, textarea, editor, checkbox, radio, select, media
//select have additional param with json array: [list="functionName(param1, param2, pram3...)"/]
//fields types: text, password, email...
function insertDynamicField($id, $title, $dynamicFields, $pattern=false, $name=false){
    if(!$pattern){
        return;
    }

    if(!$name){
        $name = $id;
    }

    echo '<p><label for="' . $id . '">'.$title.': </label>';

    $dynamicFieldsArray = json_decode($dynamicFields, true);

    echo '<div class="dynamic-fields">';
    echo '<div class="dynamic-fields-blocks">';

    if($dynamicFieldsArray){
        $num = 1;
        foreach ($dynamicFieldsArray as $item){
            echo getDynamicFieldTemplate($num, $item, $pattern, $id);
            $num++;
        }
    }

    echo '</div>';
    echo '<input class="hidden-input-data" id="'.$id.'" type="hidden" name="'.$name.'" value="'.htmlentities($dynamicFields).'">';
    echo '<span class="add-block-title">'.__('Add Block of Fields', 'winnbrain').': </span>';
    echo '<div class="add-block"><span class="dashicons dashicons-plus"></span></div>';

    echo '<div class="fieldsTemplate">';
    echo getDynamicFieldTemplate('[!num!]', false, $pattern, $id);
    echo '</div>';
    echo '</div>';

}

function getDynamicFieldTemplate($num, $item = false, $pattern = false, $blockid = false){
    global $text_domain;

    if(!$pattern || !$blockid){
        return false;
    }

    //seeking fields in pattern
    $fields = getFieldsFromPattern($pattern);
    $fieldsAndOptions = [];
    $patternMod = $pattern;

    if(count($fields)>0){
        foreach ($fields as $k => $v){
            $anchor = '[!input('.$k.')!]';
            $patternMod = str_replace($v['field'], $anchor, $patternMod);
            //find args
            $fieldsAndOptions[$k] = ['field'=>$v, 'options'=>getArgsFromPattern($v['field']), 'anchor'=>$anchor];
        }
    }

    $templateFields = [];
    $itemTitle = '';
    if(count($fieldsAndOptions)>0){
        foreach ($fieldsAndOptions as $k => $v){
            switch (@$v['field']['field_type']){
                case 'input':

                    $title = @$v['options']['title'];
                    $type = @$v['options']['type']?' type="'.$v['options']['type'].'"':'';
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $class = @$v['options']['class']?' class="'.$v['options']['class'].'"':'';

                    //protected output for field value (if set htmlentities class)
                    if(strpos($class, 'htmlentities')>-1){
                        $value = isset($item[@$v['options']['name']])?' value="'.htmlentities($item[$v['options']['name']]).'"':' value=""';
                    }
                    else{
                        $value = isset($item[@$v['options']['name']])?' value="'.$item[$v['options']['name']].'"':' value=""';
                    }

                    //set item title
                    if($v['options']['name']=='title'){
                        $itemTitle = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    }

                    $str = $title?__($title, 'winnbrain').': ':'';
                    $str .= '<input id="'.$blockid.'_'.$num.'-'.$v['options']['name'].'"'.$class.$type.$name.$value.'>';
                    $templateFields[$k] = $str;

                    break;
                case 'textarea':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $class = @$v['options']['class']?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    //set item title
                    if(strpos($class, 'fieldTitle')>-1){
                        $itemTitle = trim($value);
                    }

                    $str = __($title, 'winnbrain').': ';
                    $str .= '<textarea id="'.$blockid.'-'.$num.'-'.$v['options']['name'].'"'.$class.' rows="3"'.$name.'>'.$value.'</textarea>';
                    $templateFields[$k] = $str;

                    break;
                case 'media':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    $id = $blockid.'-'.$num.'-'.$v['options']['name'];
                    $options = @$v['options']['data']?' data-options="'.$v['options']['data'].'"':'';

                    $str = '<span class="img-title">'.__($title, 'winnbrain').': </span>';
                    $str .= '<div class="widget-block-img">';
                    if($value){
                        $mediaId = get_attachment_file_id($value);
                        $selectedMedia = '';
                        if($mediaId){
                            $selectedMedia = '<script>';
                            $selectedMedia .= 'if(!selectedMedia["'.$id.'"]){selectedMedia["'.$id.'"] = [];}';
                            $selectedMedia .= 'selectedMedia["'.$id.'"].push('.$mediaId.');';
                            $selectedMedia .= '</script>';
                        }
                        $str .= '<a class="thickbox" href="'.$value.'?TB_iframe=false&width=800&height=600"><img src="'.$value.'"></a>';
                        $str .= $selectedMedia;
                    }
                    $str .= '</div>';
                    $str .= '<input'.$options.' class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'winnbrain').'" />';
                    $str .= '<input id="'.$id.'" class="input-value datafield" type="hidden"'.$name.' value="'.$value.'" />';
                    $str .= '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'winnbrain').'" />';
                    $templateFields[$k] = $str;

                    break;
                case 'editor':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $class = ' class="addEditor';
                    $class = @$v['options']['class']?$class.' '.$v['options']['class'].'"':$class.'"';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    //set item title
                    if(strpos($class, 'fieldTitle')>-1){
                        $itemTitle = trim($value);
                    }

                    $str = __($title, 'winnbrain').': ';
                    $str .= '<textarea id="'.$blockid.'-'.$num.'-'.$v['options']['name'].'"'.$class.' rows="3"'.$name.'>'.$value.'</textarea>';
                    $templateFields[$k] = $str;

                    break;
                case 'imageslist':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    $icons = getItemsFromDynamicFields('field_contact_icons');

                    $str = __($title, 'winnbrain').': ';
                    //Dropdown plugin data
                    $dropdown = 'dropdown_'.$v['options']['name'].'_'.$num;
                    $str .= '<div id="'.$dropdown.'" class="Dropdown"></div>';
                    $datafield = 'dropdown_'.$v['options']['name'].'_'.$num.'_datafield';
                    $str .= '<input id="'.$datafield.'" class="input-value datafield" type="hidden"'.$name.' value="'.$value.'" />';
                    $str .= '<script>';
                    $str .= 'ddData["'.$dropdown.'"] = [';

                    if(count($icons)>0){
                        foreach ($icons as $val) {
                            $selected = '';
                            if($value == $val['img']){
                                $selected = 'selected: true,';
                            }
                            $str .= '{';
                            $str .=     'text: "'.$val['title'].'",';
                            $str .=     $selected;
                            $str .=     'imageSrc: "'.$val['img'].'",';
                            $str .= '},';
                        }
                    }
                    $str = substr($str, 0, strlen($str)-1);
                    $str .= ']; ';
                    $str .= '</script>';

                    $templateFields[$k] = $str;
                    break;
                case 'checkbox':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $class = @$v['options']['class']?' class="'.$v['options']['class'].'"':'';
                    $value = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';

                    $str = __($title, 'winnbrain').': ';
                    $id = $blockid.'-'.$num.'-'.$v['options']['name'];
                    $checked = '';
                    if($value == 1){
                        $checked = ' checked';
                    }
                    $str .= '<input'.$class.' id="' . $id . '" type="checkbox"'.$name.' value="1"'.$checked.'></p>';
                    $templateFields[$k] = $str;
                    break;
                case 'radio':

                    $title = @$v['options']['title'];
                    $name = isset($v['options']['name'])?' data-name="'.$v['options']['name'].'"':'';
                    $nameAttr = isset($v['options']['name'])?' name="'.$blockid.'-'.$num.'-'.$v['options']['name'].'"':'';
                    $class = isset($v['options']['class'])?' class="'.$v['options']['class'].'"':'';
                    $currentValue = isset($item[@$v['options']['name']])?$item[$v['options']['name']]:'';
                    $value = $v['options']['value']?$v['options']['value']:'';
                    $default = @$v['options']['data']=='checked'?true:false;

                    $str = __($title, 'winnbrain').': ';
                    $checked = '';
                    if($currentValue == $value || !$currentValue && $default){
                        $checked = ' checked';
                    }
                    $str .= '<input'.$class.' type="radio"'.$name.$nameAttr.' value="'.$value.'"'.$checked.'></p>';
                    $templateFields[$k] = $str;
                    break;

                case 'select':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $id = @$v['options']['name']?' id="'.$blockid.'-'.$num.'-'.$v['options']['name'].'"':'';
                    $class = @$v['options']['class']?' class="'.$v['options']['class'].'"':'';
                    $value = @$item[@$v['options']['name']]?$item[$v['options']['name']]:'';
                    $options = @$v['options']['data']?' data-options="'.$v['options']['data'].'"':'';
                    $list = @$v['options']['list']?$v['options']['list']:'';

                    $listType = '';
                    if($list && strpos($list, ':')>-1){
                        $listType = substr($list, 0, strpos($list, ':')+1);
                        $list = substr($list, strpos($list, ':')+1);
                    }

                    $listArray = [];
                    if($listType == 'func:'){
                        $func = '';
                        $optionsArray = [];
                        if($list){
                            $func = substr($list, 0, strpos($list, '('));
                            if(strpos($list, '(')>-1){
                                $opt = substr($list, strpos($list, '(')+1);
                                $opt = substr($opt, 0, strpos($opt, ')'));
                                if($opt){
                                    $optionsArray = array_map('trim',array_filter(explode(',', $opt)));
                                }
                            }
                        }
                        //get list
                        if($func){
                            $listArray = call_user_func_array($func, $optionsArray);
                        }
                    }
                    elseif($listType == 'json:'){
                        $listArray = json_decode($list, true);
                    }

                    $str = __($title, $text_domain).': ';
                    $str .= '<select'.$id.$class.' rows="3"'.$name.$options.'>';
                    $str .= '<option value="0"></option>';
                    if(count($listArray)>0){
                        foreach ($listArray as $key => $option){
                            $selected = '';
                            if($key == $value){
                                $selected = ' selected';
                            }
                            $str .= '<option value="'.$key.'"'.$selected.'>'.$option.'</option>';
                        }
                    }
                    $str .= '</select>';
                    $templateFields[$k] = $str;
                    break;

                case 'multiselect':

                    $title = @$v['options']['title'];
                    $name = @$v['options']['name']?' data-name="'.$v['options']['name'].'"':'';
                    $id = @$v['options']['name']?' id="'.$blockid.'-'.$num.'-'.$v['options']['name'].'"':'';
                    $class = @$v['options']['class']?' class="'.$v['options']['class'].'"':'';
                    $value = @$item[@$v['options']['name']]?$item[$v['options']['name']]:[];
                    $options = @$v['options']['data']?' data-options="'.$v['options']['data'].'"':'';
                    $list = @$v['options']['list']?$v['options']['list']:'';

                    $listType = '';
                    if($list && strpos($list, ':')>-1){
                        $listType = substr($list, 0, strpos($list, ':')+1);
                        $list = substr($list, strpos($list, ':')+1);
                    }

                    $listArray = [];
                    if($listType == 'func:'){
                        $func = '';
                        $optionsArray = [];
                        if($list){
                            $func = substr($list, 0, strpos($list, '('));
                            if(strpos($list, '(')>-1){
                                $opt = substr($list, strpos($list, '(')+1);
                                $opt = substr($opt, 0, strpos($opt, ')'));
                                if($opt){
                                    $optionsArray = array_map('trim',array_filter(explode(',', $opt)));
                                }
                            }
                        }
                        //get list
                        if($func){
                            $listArray = call_user_func_array($func, $optionsArray);
                        }
                    }
                    elseif($listType == 'json:'){
                        $listArray = json_decode($list, true);
                    }

                    $str = __($title, 'winnbrain').': ';
                    $str .= '<select'.$class.$id.$name.$options.'" multiple="multiple">';
                    if(count($listArray)>0){
                        $str .= '<option value="0"></option>';
                        foreach ($listArray as $key => $val){
                            $selected = '';
                            if(count($value)>0){
                                foreach ($value as $v){
                                    if($v == $key){
                                        $selected = ' selected';
                                        break;
                                    }
                                }
                            }
                            $str .= '<option value="'.$key.'"'.$selected.'>'.$val.'</option>';
                        }
                    }
                    $str .= '</select>';
                    $templateFields[$k] = $str;
                    break;
            }
        }
    }

    $template = '';
    $template .= '<div data-num="'.$num.'" class="dynamic-fields-block-wrap">';
    $template .= '<div data-num="'.$num.'" class="dynamic-fields-block">';
    $template .= '<input class="datafield" type="hidden" data-name="item_id" value="'.(isset($item['item_id'])?$item['item_id']:'[!id!]').'">';
    $template .= '<div class="top-wrap">';
    $template .= '<div class="block-num">#'.$num.'</div><div class="block-toggle"><span class="dashicons dashicons-arrow-up"></span></div>';
    $itemTitle = cutString(strip_tags($itemTitle), $count=45, $end='...');
    $template .= $itemTitle?'<span class="itemTitle">'.$itemTitle.'</span>':'';
    $template .= '<div class="remove-block"><span class="dashicons dashicons-no"></span></div>';
    $template .= '</div>';
    $template .= '<div class="content-wrap">';

    $templateStr = '';
    if($patternMod && count($templateFields)>0){
        $templateStr = $patternMod;
        foreach ($templateFields as $k => $v){
            $templateStr = str_replace('[!input('.$k.')!]', $v, $templateStr);
        }
    }

    $template .= $templateStr;
    $template .= '<div data-num="'.$num.'" class="change-pos"><span data-act="up" class="dashicons dashicons-arrow-up-alt2"></span><span data-act="down" class="dashicons dashicons-arrow-down-alt2"></span></div>';
    $template .= '</div>';
    $template .= '</div>';
    $template .= '</div>';

    return $template;
}
//return extracted fields from pattern
function getFieldsFromPattern($pattern){
    if(!$pattern){
        return false;
    }
    $fieldsList = [
        'input' => '<input ',
        'textarea' => '<textarea ',
        'editor' => '<editor ',
        'checkbox' => '<checkbox ',
        'radio' => '<radio ',
        'select' => '<select ',
        'multiselect' => '<multiselect ',
        'media' => '<media ',
        'imageslist' => '<imageslist ',
    ];
    $fields = [];
    foreach ($fieldsList as $k => $v){
        $tag = $v;
        $startPoint = 0;
        $go = true;
        while($go){
            $patternRes = substr($pattern, $startPoint);
            $start = strpos($patternRes, $tag);
            if($start){
                $input = substr($patternRes, $start);
                $end = strpos($input, ' />')+3;
                $input = substr($patternRes, $start, $end);
                $fields[] = ['field'=>$input,'field_type'=>$k];
                $startPoint = $startPoint+$start+$end;
            }
            else{
                $go = false;
            }
        }
    }
    return $fields;
}
//return arguments from extracted fields
function getArgsFromPattern($v){
    $args = [
        'title' => '',
        'type' => '',
        'name' => '',
        'class' => '',
        'data' => '',
        'value' => '',
        'list' => ''
    ];
    if($v){
        //title
        $find = '[title="';
        $start = strpos($v, $find);
        if($start){
            $args['title'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //type
        $find = '[type="';
        $start = strpos($v, $find);
        if($start){
            $args['type'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //name
        $find = '[name="';
        $start = strpos($v, $find);
        if($start){
            $args['name'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //class
        $find = '[class="';
        $start = strpos($v, $find);
        if($start){
            $args['class'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //value
        $find = '[value="';
        $start = strpos($v, $find);
        if($start){
            $args['value'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //data
        $find = '[data="';
        $start = strpos($v, $find);
        if($start){
            $args['data'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
        //list
        $find = '[list="';
        $start = strpos($v, $find);
        if($start){
            $args['list'] = substr($v, $start+strlen($find), strpos(substr($v, $start+strlen($find)), '"/]'));
        }
    }
    return $args;
}
//get dynamic field items by its variable id
function getItemsFromDynamicFields($dynamicFieldsId, $list=false, $title="title"){
    if($dynamicFieldsId){
        $dynamicFieldsJson = get_option($dynamicFieldsId);
        if($dynamicFieldsJson){
            $dynamicFieldsArr = json_decode($dynamicFieldsJson, true);
            if($list && is_array($dynamicFieldsArr) && count($dynamicFieldsArr)>0){
                $resultArray = [];
                foreach($dynamicFieldsArr as $key => $item){
                    $resultArray[$item['item_id']] = $item[$title];
                }
                return $resultArray;
            }
            return $dynamicFieldsArr?$dynamicFieldsArr:[];
        }
    }
    return [];
}

//gets widget settings by its id
function get_widget_instance($widget_id) {
    global $wp_registered_widgets;
    if ( empty( $wp_registered_widgets[$widget_id]['callback'] ) )
        return array();
    /** @var WP_Widget $widget */
    $widget = $wp_registered_widgets[$widget_id]['callback'][0];
    $settings = $widget->get_settings();
    return ! empty( $settings[$widget->number] ) ? $settings[$widget->number] : array();
}

//gets widget dynamic field items by its variable id. if list param is set then will be returned key=>title
function getItemsFromWidgetDynamicFields($widget_id, $dynamicFieldsId, $list=false, $title="title"){
    if($widget_id && $dynamicFieldsId){
        $widget = get_widget_instance($widget_id);
        $dynamicFieldsJson = '';
        if(is_array($widget) && isset($widget[$dynamicFieldsId])){
            $dynamicFieldsJson = $widget[$dynamicFieldsId];
        }
        if($dynamicFieldsJson){
            $dynamicFieldsArr = json_decode($dynamicFieldsJson, true);
            if($list && is_array($dynamicFieldsArr) && count($dynamicFieldsArr)>0){
                $resultArray = [];
                foreach($dynamicFieldsArr as $key => $item){
                    $resultArray[$item['item_id']] = $item[$title];
                }
                return $resultArray;
            }
            return $dynamicFieldsArr?$dynamicFieldsArr:[];
        }
    }
    return [];
}

//add scripts to admin head
function winnbrain_custom_admin_head() {
    echo '<script type="text/javascript">
        var ddData = {};
        var selectedMedia = {};
        var mediaLibrary = {};
        </script>';
}
add_action( 'admin_head', 'winnbrain_custom_admin_head' );

//Add responsive container to embeds
function winnbrain_embed_html( $html ) {
    return '<div class="video-container iframe-basic-box">' . $html . '</div>';
}
add_filter( 'embed_oembed_html', 'winnbrain_embed_html', 10, 3 );

//custom gallery output
add_filter( 'post_gallery', 'custom_gallery', 10, 2 );
function custom_gallery($output, $attr) {
    $attrArr = isset($attr['ids'])?explode(',', $attr['ids']):[];
    $str = '';
    $str .= '<div class="slider-1-item slider-basic-content">';
    if(count($attrArr)>0){
        foreach ($attrArr as $id) {
            $img = wp_get_attachment_url($id);
            if ($img) {
                $str .= '<div class="item">';
                $str .= '<img src="'.$img.'" alt="">';
                $str .= '</div>';
            }
        }
    }
    $str .= '</div>';
    return $str;
}

function custom_format_TinyMCE( $in ) {
    $in['wpautop'] = false;
    return $in;
}
add_filter( 'tiny_mce_before_init', 'custom_format_TinyMCE' );

//hide updates notifications for inactive plugins
function update_active_plugins($value = '') {
    if ((isset($value->response)) && (count($value->response))) {
        // Get the list cut current active plugins
        $active_plugins = get_option('active_plugins');
        if ($active_plugins) {
            //  Here we start to compare the $value->response
            //  items checking each against the active plugins list.
            foreach($value->response as $plugin_idx => $plugin_item) {
                // If the response item is not an active plugin then remove it.
                // This will prevent WordPress from indicating the plugin needs update actions.
                if (!in_array($plugin_idx, $active_plugins))
                    unset($value->response[$plugin_idx]);
            }
        }
        else {
            // If no active plugins then ignore the inactive out of date ones.
            foreach($value->response as $plugin_idx => $plugin_item) {
                unset($value->response);
            }
        }
    }
    return $value;
}
add_filter('site_transient_update_plugins', 'update_active_plugins');

//SEO----------------------------------
add_action('add_meta_boxes', 'getSeoFields', 10, 2);
function getSeoFields($post_type, $post){
    global $text_domain;

    if($post_type == 'contacts' || $post_type == 'testimonials'){
        return;
    }
    add_meta_box('seo_settings', __('SEO', $text_domain), 'seo_settings_callback', $post_type, 'normal', 'default');
}
function seo_settings_callback($post){
    global $text_domain;

    wp_nonce_field('seo_settings_save_data', 'seo_settings_meta_box_nonce');//add unique verifying field
    $title = get_post_meta($post->ID, '_seo_settings_title', true);//get custom meta box
    $desc = get_post_meta($post->ID, '_seo_settings_desc', true);//get custom meta box
    $keywords = get_post_meta($post->ID, '_seo_settings_keywords', true);//get custom meta box
    //title
    echo '<p>'.__('Title', $text_domain).': </p>';
    echo '<p><input id="seo_settings_title" class="widefat datafield" title="'.__('Title', $text_domain).'" type="text" name="seo_settings_title" value="'.$title.'" /></p>';
    //description
    echo '<p>'.__('Description', $text_domain).': </p>';
    echo '<p><textarea id="seo_settings_desc" class="widefat datafield" title="'.__('Description', $text_domain).'" name="seo_settings_desc">'.$desc.'</textarea></p>';
    //keywords
    echo '<p>'.__('Keywords', $text_domain).': </p>';
    echo '<p><textarea id="seo_settings_keywords" class="widefat datafield" title="'.__('Keywords', $text_domain).'" name="seo_settings_keywords">'.$keywords.'</textarea></p>';
}
function seo_settings_save_data($post_id){
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['seo_settings_meta_box_nonce']) &&
        wp_verify_nonce($_POST['seo_settings_meta_box_nonce'], 'getSeoFields')){//if user doesn't have permission, don't save
        return;
    }
    //title
    if(isset($_POST['seo_settings_title'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_title'];
        update_post_meta($post_id, '_seo_settings_title', $my_data);//save data from meta box field
    }
    //desc
    if(isset($_POST['seo_settings_desc'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_desc'];
        update_post_meta($post_id, '_seo_settings_desc', $my_data);//save data from meta box field
    }
    //keywoeds
    if(isset($_POST['seo_settings_keywords'])){//check is meta box verifying exist
        $my_data = $_POST['seo_settings_keywords'];
        update_post_meta($post_id, '_seo_settings_keywords', $my_data);//save data from meta box field
    }
}
add_action('save_post', 'seo_settings_save_data');//save custom meta box
//---------------------------------------------------

//PAGINATION-----------------------------------------
function combineUrlQueries(Array $query=[]){
    $res = '';
    if(count($query)>0){
        foreach ($query as $k => $v){
            if($res){
                $res .= '&';
            }
            $res .= $k.'='.$v;
        }
    }
    return $res;
}
//returns content with offset and variables for pagination. takes: posts array, items amount to display per page, pagination items count
function getPaged($posts_array, $perPage=6, $maxPages=5){
    $res = [];
    if(!is_array($posts_array) || count($posts_array)<1){
        $res['posts_array'] = [];
        return $res;
    }
    $res['perPage'] = $perPage;
    $res['maxPages'] = $maxPages;
    $res['posts_array'] = $posts_array;
    //$res['page'] = isset($_GET['page'])&&$_GET['page']>1?(int)$_GET['page']:1;
    $res['page'] = get_query_var('page')&&get_query_var('page')>1?(int)get_query_var('page'):1;
    $res['offset'] = $res['perPage']*($res['page']-1);
    $res['pagesCount'] = count($res['posts_array']);
    $res['pagesCount'] = ceil($res['pagesCount']/$res['perPage']);
    $res['maxPages'] = $res['maxPages']>$res['pagesCount']?$res['pagesCount']:$res['maxPages'];
    $res['posts_array'] = array_slice($res['posts_array'], $res['offset'], $res['perPage']);
    $res['currUrl'] = "//".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $res['currUrlArr'] = parse_url($res['currUrl']);
    $res['$urlQuery'] = @$res['currUrlArr']['query'];
    $res['urlQueryArr'] = [];
    $res['urlQueryStr'] = '';
    if($res['$urlQuery']){
        $_urlQueryArr = explode('&', $res['$urlQuery']);
        $_urlQueryArr = array_filter($_urlQueryArr);
        if(count($_urlQueryArr)>0){
            foreach ($_urlQueryArr as $v){
                $_arr = explode('=', $v);
                $res['urlQueryArr'][$_arr[0]] = $_arr[1];
            }
        }
    }
    if(isset($res['urlQueryArr']['page'])){
        unset($res['urlQueryArr']['page']);
        $res['urlQueryArr'] = array_filter($res['urlQueryArr']);
    }
    $res['urlQueryStr'] = combineUrlQueries($res['urlQueryArr']);
    $res['finUrl'] = @$res['urlQueryArr']['path'].($res['urlQueryStr']?'?'.$res['urlQueryStr'].'&':'?');
    return $res;
}
//returns pagination. takes: getPaged function result
function getPaganation($res){
    ?>
    <!--begin pagination-->
    <?php if(count($res['posts_array'])>0 && $res['pagesCount']>1): ?>
        <div class="container">
            <nav class="col-xs-12">
                <nav aria-label="Page navigation" class="page-pagination">
                        <ul class="pagination">
                            <?php
                            $prev = ($res['page']-1)>1?($res['page']-1):1;
                            echo '<li><a href="'.$res['finUrl'].'page='.$prev.'" aria-label="Previous"><span aria-hidden="true"></span></a></li>';
                            if($res['page']<=floor($res['maxPages']/2)){
                                $pOffset = 1;
                            }
                            else{
                                $pOffset = $res['page']-(floor($res['maxPages']/2));
                            }
                            if(($pOffset+$res['maxPages']-1)>$res['pagesCount']){
                                $pOffset = $res['pagesCount']-$res['maxPages']+1;
                            }
                            for($i=$pOffset; $i<($res['maxPages']+$pOffset); $i++){
                                $active = '';
                                if((int)$res['page'] == (int)$i){
                                    $active = ' class="active"';
                                }
                                echo '<li'.$active.'><a href="'.$res['finUrl'].'page='.$i.'">'.$i.'</a></li>';
                            }
                            $next = ($res['page']+1)>$res['pagesCount']?$res['pagesCount']:($res['page']+1);
                            echo '<li><a href="'.$res['finUrl'].'page='.$next.'" aria-label="Next"><span aria-hidden="true"></span></a></li>';
                            ?>
                        </ul>
                </nav>
            </div>
        </div>
    <?php endif; ?>
    <!--end pagination-->
    <?php
}
//-------------------------------------------------------------

$currentTemplate = '';
//get current template
function current_template_redirect( $template ) {
    global $currentTemplate;
    $currentTemplate = substr($template, strrpos($template, '/')+1);
    return $template;
}
add_filter( 'template_include', 'current_template_redirect', 99 );

//checking whether exists the categories and/or posts by user requests
function checkUrlPath($request=null, $taxonomy=null, $query=null, $type=null){
    if($request && $taxonomy && $query && $type){
        $currRequestSlug = @$query->query[$taxonomy];
        $currCatSlug = @$query->query_vars[$taxonomy];
        if(strrpos($currRequestSlug, '/')>0){
            $currCatSlug = substr($currRequestSlug, strrpos($currRequestSlug, '/')+1);
        }
        if(!$currCatSlug && !$currRequestSlug){
            return false;
        }
        $terms = get_terms( array(
            'taxonomy' => $taxonomy,
            'hide_empty' => false
        ));
        $currcat = null;
        if(count($terms)>0){
            foreach ($terms as $term){
                if($term->slug === $currCatSlug){
                    $currcat = $term;
                    break;
                }
            }
        }
        $parents = [];
        if($currcat){
            $parents = get_ancestors($currcat->term_id, $taxonomy, 'taxonomy');
            $parents = array_reverse($parents);
        }
        $terms = get_terms($taxonomy, array('hide_empty' => false));
        $path = '';
        if(count($parents)>0 && count($terms)>0){//if have parents
            foreach ($parents as $v){
                foreach ($terms as $v2){
                    if($v2->term_id === $v){
                        $path .= $v2->slug.'/';
                    }
                }
            }
            $path = $path.$currcat->slug;
        }
        elseif(!count($parents)>0 && $currcat){//if no parents
            $path = $currcat->slug;
        }
        else{//if post without category
            if($currCatSlug === 'uncategorized'){
                $request_str = str_replace('_', '', substr($request, strrpos($request, '/')+1));
                if($request_str != $query->post->post_name){
                    return false;
                }
                $ischild = get_the_terms($query->post->ID, $taxonomy);
                if(!$ischild){
                    return true;
                }
            }
            return false;
        }
        if($type == 'category' && $path === $currRequestSlug){
            return true;
        }
        elseif($type == 'post' && $path === $currRequestSlug && isset($query->post)){
            $request_str = str_replace('_', '', substr($request, strrpos($request, '/')+1));
            if($request_str != $query->post->post_name){
                return false;
            }
            $ischild = get_the_terms($query->post->ID, $taxonomy);
            $ischild = @$ischild[0]->slug===$currCatSlug?true:false;
            if($ischild){
                return true;
            }
        }
    }
    return false;
}

//takes arguments from post_type_link filter and return modified correct link with categories hierarchy
function post_link( $post_link, $post, $leavename, $sample, $post_type_category ){
    if ( is_object( $post ) ){
        $terms = wp_get_object_terms( $post->ID, $post_type_category );
        $currcat = isset($terms[0])?$terms[0]:null;
        $parents = null;
        if($currcat){
            $parents = get_ancestors( $currcat->term_id, $post_type_category, 'taxonomy' );
            $parents = array_reverse($parents);
        }
        $terms = get_terms($post_type_category, array('hide_empty' => false));
        $path = '';
        if(count($parents)>0 && count($terms)>0){
            foreach ($parents as $v){
                foreach ($terms as $v2){
                    if($v2->term_id === $v){
                        $path .= $v2->slug.'/';
                    }
                }
            }
            if($path){
                $path .= $currcat->slug;
                $separator = $path?'_':'';
                $str = str_replace( '%cat%/' , $path.'/'.$separator , $post_link );
                return $str;
            }
        }
        elseif(!count($parents)>0 && $currcat){
            $path = $currcat->slug;
            $separator = $path?'_':'';
            $str = str_replace( '%cat%/' , $path.'/'.$separator , $post_link );
            return $str;
        }
        else{
            $separator = $post_link?'_':'';
            $str = str_replace( '%cat%/' , 'uncategorized/'.$separator , $post_link );
            return $str;
        }
    }
    return $post_link;
}

//add hierarchy to custom post
function post_path($wp_query, $wp, $adminka, $thisCustomPost){
    $currentRequest = strpos($wp->request, '/')?substr($wp->request, 0, strpos($wp->request, '/')):$wp->request;
    if($adminka != $currentRequest && $thisCustomPost == $currentRequest){
        $type = null;
        if(isset($wp_query->query['name'])){
            //post
            $type = 'post';
        }
        elseif(count($wp_query->posts)>0 && !isset($wp_query->queried_object)) {
            //category with posts
            $type = 'category';
        }
        elseif(count($wp_query->posts)<=0 && isset($wp_query->queried_object)) {
            //category without posts
            $type = 'category';
        }
        else{
            force_404();
        }
        if($wp->request != $thisCustomPost){
            if(!checkUrlPath($wp->request, 'category_'.$thisCustomPost, $wp_query, $type)){
                force_404();
            }
        }
        else{
            //force_404();
        }
    }
}

//set correct path to post including categories hierarchy
add_action( 'wp', 'post_loading' );
function post_loading(){
    global $wp_query;
    global $wp;
    global $thisCustomPosts;
    if(isset($thisCustomPosts[get_post_type()])){
        $adminka = 'wp-admin';
        $thisCustomPost = get_post_type();
        post_path($wp_query, $wp, $adminka, $thisCustomPost);
    }
}

//return modified link with categories hierarchy
function get_post_link( $post_link, $post, $leavename, $sample ){
    global $thisCustomPosts;
    $post_type_category = null;
    if(isset($thisCustomPosts[get_post_type()])){
        $post_type_category = 'category_'.get_post_type();
    }
    elseif($post->post_type && isset($thisCustomPosts[$post->post_type])){
        $post_type_category = 'category_'.$post->post_type;
    }
    if($post_type_category){
        return post_link( $post_link, $post, $leavename, $sample, $post_type_category);
    }
    return $post_link;
}
add_filter( 'post_type_link', 'get_post_link', 10, 4 );

function getCoordinates($address){
    $address = str_replace(" ", "+", trim(esc_attr($address)));
    $key = get_option('field_mapKey');
    $key = $key?$key:'AIzaSyDchUM7N2SWlrtxSzQ5GwvSQzOUl3vt1ZM';
    $url = "https://maps.google.com/maps/api/geocode/json?sensor=false&address=$address&key=".$key;
    $response = file_get_contents($url);
    $json = json_decode($response,TRUE);
    if(isset($json['results'][0]['geometry']['location']['lat']) && isset($json['results'][0]['geometry']['location']['lng'])){
        return ($json['results'][0]['geometry']['location']['lat'].",".$json['results'][0]['geometry']['location']['lng']);
    }
    return '';
}

//DYNAMIC SIDEBARS----------------------------------------
//add dynamic sidebars
function addDynamicSidebars(){
    $dynamicFields = get_option('field_custom_sidebars');
    if($dynamicFields){
        $custom_dynamic_sidebars = json_decode($dynamicFields, true);
        if(count($custom_dynamic_sidebars)>0){
            foreach ($custom_dynamic_sidebars as $sidebar){
                $hash = substr(strtolower(preg_replace('/[0-9_\/]+/','',base64_encode(sha1($sidebar['item_id'])))),0,8);
                register_sidebar([
                        'id' => $hash,
                        'name' => $sidebar['title'],
                        'description' => $sidebar['desc'],
                        'type' => 'dynamic'
                ]);
            }
        }
    }
}
add_action('widgets_init', 'addDynamicSidebars');
//display dynamic sidebars
add_action('add_meta_boxes', 'displayDynamicSidebars', 10, 2);
function displayDynamicSidebars($post_type, $post){
    global $text_domain;
    if($post_type != 'contacts'){
        add_meta_box('custom_sidebars_settings', __('Custom Sidebars', $text_domain), 'custom_sidebars_callback', $post_type, 'side', 'default');
    }
}
function custom_sidebars_callback($post){
    global $text_domain;
    wp_nonce_field('custom_sidebars_save_data', 'custom_sidebars_meta_box_nonce');//add unique verifying field
    $sidebars = get_post_meta($post->ID, '_custom_sidebars_sidebars', true);//get custom meta box
    $position = get_post_meta($post->ID, '_custom_sidebars_position', true);//get custom meta box

    //sidebars
    $dynamSidebarsList = getSidebars(true);
    echo '<p>'.__('Sidebars', $text_domain).':</p>';
    echo '<select class="widefat" id="custom_sidebars_sidebars" multiple="multiple" name="custom_sidebars_sidebars[]">';
    if(count($dynamSidebarsList)>0){
        echo '<option value="0"></option>';
        foreach ($dynamSidebarsList as $key => $item){
            $selected = '';
            if(is_array($sidebars) && count($sidebars)>0){
                foreach ($sidebars as $v){
                    if($v == $key){
                        $selected = ' selected';
                        break;
                    }
                }
            }
            echo '<option value="'.$key.'"'.$selected.'>'.$item.'</option>';
        }
    }
    echo '</select>';

    //position
    echo '<p>'.__('Positions', $text_domain).':</p>';
    echo '<select class="widefat" id="custom_sidebars_position" name="custom_sidebars_position">';
        echo '<option value="bottom"'.($position=='bottom'?' selected':'').'>'.__('Bottom', $text_domain).'</option>';
        echo '<option value="top"'.($position=='top'?' selected':'').'>'.__('Top', $text_domain).'</option>';
    echo '</select>';

}
function custom_sidebars_save_data($post_id){
    if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE){//if wp doing auto-save, prevent saving meta box
        return;
    }
    if(!current_user_can('edit_post', $post_id)){//if user doesn't have permission, don't save
        return;
    }
    if(isset($_POST['custom_sidebars_meta_box_nonce']) &&
        wp_verify_nonce($_POST['custom_sidebars_meta_box_nonce'], 'displayDynamicSidebars')){//if user doesn't have permission, don't save
        return;
    }
    //sidebars
    if(isset($_POST['custom_sidebars_sidebars'])){//check is meta box verifying exist
        $my_data = $_POST['custom_sidebars_sidebars'];
        update_post_meta($post_id, '_custom_sidebars_sidebars', $my_data);//save data from meta box field
    }
    //position
    if(isset($_POST['custom_sidebars_position'])){//check is meta box verifying exist
        $my_data = $_POST['custom_sidebars_position'];
        update_post_meta($post_id, '_custom_sidebars_position', $my_data);//save data from meta box field
    }
}
add_action('save_post', 'custom_sidebars_save_data');//save custom meta box
function addSidebars($post_id, $called_position='bottom'){
    if($post_id){
        $sidebars = get_post_meta($post_id, '_custom_sidebars_sidebars', true);
        $position = get_post_meta($post_id, '_custom_sidebars_position', true);
        if($called_position == $position && is_array($sidebars) && count($sidebars)>0){
            foreach ($sidebars as $item){
                if(function_exists('dynamic_sidebar') && is_active_sidebar($item)){
                    dynamic_sidebar($item);
                }
            }
        }
    }
}
function getSidebars($list=false){
    global $wp_registered_sidebars;
    if(!$list){
        return $wp_registered_sidebars;
    }
    $dynamSidebarsList = [];
    if(count($wp_registered_sidebars)>0){
        foreach ($wp_registered_sidebars as $item){
            $dynamSidebarsList[$item['id']] = $item['name'];
        }
    }
    return $dynamSidebarsList;
}
//--------------------------------------------------------

//returns number from passed string with type as int or float
function getNumberFromString($str){
    if(is_string($str)){
        preg_match('/^.*?([\d]+(?:\.[\d]+)?).*?$/', $str, $match);
        $match = @$match[1];
        if(is_float($match+0)){
            $match = (float)$match;
        }
        else{
            $match = (int)$match;
        }
        return $match;
    }
    return false;
}

/**
 * Add support for the QuickTime (.mov) video format.
 */
add_filter( 'wp_video_extensions',
    function( $exts ) {
        $exts[] = 'mov';
        return $exts;
    }
);

//return array with sort methods for select tags
function getSortOrderBy(){
    return [
        'date' => 'date',
        'modified' => 'modified',
        'rand' => 'rand',
        'title' => 'title',
        'ID' => 'ID',
        'content' => 'content',
        'name' => 'name',
        'meta_value' => 'meta_value'
    ];
}
function getSortOrder(){
    return [
        'DESC' => 'DESC',
        'ASC' => 'ASC'
    ];
}

//cut string by passed letters amount. can be added ending for cut strings
function cutString($string, $count=20, $end='...'){
    $result = '';
    if(is_string($string) && mb_strlen(trim($string))>0){
        $contentCount = mb_strlen($string);
        $result = mb_substr($string, 0, $count);
        if($contentCount>mb_strlen($result)){
            $result = trim($result).$end;
        }
    }
    return $result;
}

//takes items array and returns array formatted like: id=>val
function getItemsList($arr, $id='item_id', $val='title'){
    $result = [];
    if(is_array($arr) && count($arr)>0){
        foreach ($arr as $item){
            $result[$item[$id]] = $item[$val];
        }
    }
    return $result;
}

//add new image thumbnails
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'block-image', 400, 400 );
    add_image_size( 'block-image-medium', 800, 800 );
    add_image_size( 'block-image-large', 1024, 1024 );
}

//removing default widgets
add_action( 'widgets_init', 'cwwp_unregister_default_widgets' );
function cwwp_unregister_default_widgets() {
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Categories' );
    unregister_widget( 'WP_Nav_Menu_Widget' );
    unregister_widget( 'WP_Widget_Links' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Search' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    //unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_Media_Gallery' );
    //unregister_widget( 'WP_Widget_Media_Image' );
    //unregister_widget( 'WP_Widget_Media_Video' );
    //unregister_widget( 'WP_Widget_Media_Audio' );
    //unregister_widget( 'WP_Widget_Custom_HTML' );
}

//
function getPolylangCurrentLang($returnAsUrl = false){
    $res = '/';
    if(function_exists('pll_current_language') && function_exists('pll_default_language')){
        $currantLang = pll_current_language();
        if(pll_default_language() != $currantLang){
            $res = '/'.$currantLang.'/';
        }
    }
    return $returnAsUrl?get_site_url().$res:$res;
}