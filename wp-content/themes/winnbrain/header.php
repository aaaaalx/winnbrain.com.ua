<?php
$post_format = get_post_type();
$bodyClass = '';
global $currentTemplate;

if($currentTemplate=='category.php'){
    $bodyClass = ' class="category-page"';
}
elseif($currentTemplate=='page-main.php'){
    $bodyClass = ' class="main-page"';
}
elseif($currentTemplate=='taxonomy-category_testimonials.php' ||
    $currentTemplate=='page-testimonials.php' ||
    $post_format=='testimonials'){
    $bodyClass = ' class="testimonials-page"';
}
elseif($currentTemplate=='taxonomy-category_employees.php' ||
    $currentTemplate=='page-employees.php' ||
    $post_format=='employees'){
    $bodyClass = ' class="employess-page"';
}
elseif($currentTemplate=='taxonomy-category_payment.php' ||
    $currentTemplate=='page-payment.php' ||
    $post_format=='payment'){
    $bodyClass = ' class="employess-page"';
}

$currantUrl = getPolylangCurrentLang(true);

?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="<?php bloginfo('charset') ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/Winnbrain_favicon_32x32_v1-1.ico" type="image/x-icon" sizes="32x32">
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/Winnbrain_favicon_64x64_v1-1.ico" type="image/x-icon" sizes="64x64">

    <?php
    $title = '';
    $desc = '';
    $keywords = '';
    if($post){
        $title = get_post_meta($post->ID, '_seo_settings_title', true);
        $title = $title?$title:wp_title("", false);//get custom meta box
        $title = $title?$title:$post->post_title;
        $desc = get_post_meta($post->ID, '_seo_settings_desc', true);//get custom meta box
        $keywords = get_post_meta($post->ID, '_seo_settings_keywords', true);//get custom meta box
    }
    ?>

    <title><?= $title; ?></title>
    <?php if($desc): ?>
        <meta name="description" content="<?= $desc; ?>">
    <?php endif; ?>
    <?php if($keywords): ?>
        <meta name="keywords" content="<?= $keywords; ?>">
    <?php endif; ?>

    <?php if(is_singular() && pings_open(get_queried_object())): ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url') ?>">
    <?php endif; ?>

    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('head')): ?>
        <?php dynamic_sidebar('head') ?>
    <?php endif; ?>

    <?php wp_head(); ?>

</head>
<body<?= $bodyClass;?>>
    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('body')): ?>
        <?php dynamic_sidebar('body') ?>
    <?php endif; ?>
<!--begin header-->
    <header>
        <div class="top-header-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="email-box icon-box">
                            <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('header_emails')): ?>
                                <?php dynamic_sidebar('header_emails') ?>
                            <?php endif; ?>
                        </div>
                        <div class="phone-box icon-box">
                            <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('header_phones')): ?>
                                <?php dynamic_sidebar('header_phones') ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-header-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1>
                            <a href="<?= $currantUrl; ?>" class="logo-header"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-header.png" alt="Winn Brain"></a>
                        </h1>
                        <button class="preheader-button">i</button>
                        <button class="menu-collapse-button"><span></span><span></span><span></span></button>
                        <div class="nav-panel">
                            <div class="search-box">
                                <form class="navbar-form search-form" method="get" action="<?= $currantUrl; ?>">
                                    <div class="form-group">
                                        <input type="text" name="s" class="form-control search-input" placeholder="Поиск">
                                    </div>
                                    <button type="submit" class="btn btn-default search-button"></button>
                                </form>
                            </div>
                            <div class="menu-list">

                                <?php
                                $defaults = array(
                                    'theme_location'  => 'main_menu',
                                    'menu'            => '',
                                    'container'       => false,
                                    'container_class' => '',
                                    'container_id'    => '',
                                    'menu_class'      => '',
                                    'menu_id'         => '',
                                    'echo'            => true,
                                    'fallback_cb'     => '',
                                    'before'          => '',
                                    'after'           => '',
                                    'link_before'     => '',
                                    'link_after'      => '',
                                    'items_wrap'      => '<ul>%3$s</ul>',
                                    'depth'           => 0
                                );
                                wp_nav_menu($defaults);
                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
<!--end header-->