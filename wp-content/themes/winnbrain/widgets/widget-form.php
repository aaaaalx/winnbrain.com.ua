<?php
class WinnbrainFormWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Form-widget', __('Winnbrain Form | Widget', 'winnbrain'),
           ['description' => __('Contact Form', 'winnbrain')]);
    }

    public function form($instance) {
        $title = '';
        $desc = '';
        $formTitle = '';
        $background = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $title = esc_attr($instance['title']);
            $desc = do_shortcode(esc_attr($instance['desc']));
            $formTitle = esc_attr($instance['formTitle']);
            $background = esc_attr($instance['background']);
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('title');
        $Name = $this->get_field_name('title');
        echo '<p><label for="' . $Id . '">' . __('Title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $title . '"></p>';

        $Id = $this->get_field_id('desc');
        $Name = $this->get_field_name('desc');
        echo '<label for="' . $Id . '">'.__( 'Description', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat" rows="6" id="' . $Id . '" name="' . $Name . '">' . $desc . '</textarea></p>';

        $Id = $this->get_field_id('formTitle');
        $Name = $this->get_field_name('formTitle');
        echo '<p><label for="' . $Id . '">' . __('Form title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $formTitle . '"></p>';

        $Id = $this->get_field_id('background');
        $Name = $this->get_field_name('background');
        echo '<p><label for="' . $Id . '">' . __('Background', 'winnbrain') . ': </label>';
        echo '<div class="widget-block-img">';
        if($background){
            $mediaId = get_attachment_file_id($background);
            $selectedMedia = '';
            if($mediaId){
                $selectedMedia = '<script>';
                $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');';
                $selectedMedia .= '</script>';
            }
            add_thickbox();
            echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
            echo $selectedMedia;
        }
        echo '</div>';
        echo '<input data-options="multiple:false,type:image,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'winnbrain').'" />';
        echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
        echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'winnbrain').'" />';
        echo '</p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = $newInstance['title'];
        $values['desc'] = $newInstance['desc'];
        $values['formTitle'] = $newInstance['formTitle'];
        $values['background'] =  htmlentities($newInstance['background']);
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $title = $instance['title'];
        $desc = do_shortcode($instance['desc']);
        $formTitle = esc_attr($instance['formTitle']);
        $background = $instance['background'];
        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if($background){
            $background = ' style="background-image: url('.$background.');"';
        }

        if(count($instance)>0) {

            ?>

            <section<?= $sectionId; ?> class="section-base section-offer so-1"<?= $background; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-md-offset-5 col-xs-12 col-xs-offset-0">
                            <div class="offer-box">
                                <?php if($title): ?>
                                <div class="offer-title gold-title">
                                    <h2><?= $title; ?></h2>
                                </div>
                                <?php endif; ?>
                                <?php if($desc): ?>
                                <div class="offer-text">
                                    <p><?= $desc; ?></p>
                                </div>
                                <?php endif; ?>
                                <form data-url="<?= admin_url('admin-ajax.php'); ?>" class="form form-1" method="post">
                                    <input type="hidden" data-form="form-1" class="input-name hidden-data" value="<?= $formTitle; ?>">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-formTitle username" name="username" required placeholder="<?= __('Your name', 'winnbrain') ?>">
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-phone phone" name="phone" required id="input-phone-1" placeholder="<?= __('Your phone', 'winnbrain') ?>">
                                        </div>
                                        <div class="col-xs-12">
                                            <button type="submit" class="main-button blue-button"><span><?= __('to get the consultation', 'winnbrain') ?></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainFormWidget");
});