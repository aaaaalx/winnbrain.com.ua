<?php
class WinnbrainFaqWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Faq-widget', __('Winnbrain Faq | Widget', $text_domain),
           ['description' => __('Faq', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;

        $sectionTitle = '';
        $shortDesc = '';
        $link = '';
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);
            $shortDesc = esc_attr($instance['shortDesc']);
            $link = $instance['link'];
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        $Id = $this->get_field_id('shortDesc');
        $Name = $this->get_field_name('shortDesc');
        echo '<label for="' . $Id . '">'.__( 'Description', $text_domain ).': </label>';
        echo '<textarea class="widefat" rows="3" id="' . $Id . '" name="' . $Name . '">' . $shortDesc . '</textarea></p>';

        $Id = $this->get_field_id('link');
        $Name = $this->get_field_name('link');
        echo '<label for="' . $Id . '">'.__( 'Link', $text_domain ).': </label>';
        echo '<textarea class="widefat linkOnly getLinksList" rows="3" id="' . $Id . '" name="' . $Name . '">' . $link . '</textarea></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><editor [class="widefat datafield fieldTitle"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><editor [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', $text_domain), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['sectionTitle'] = esc_attr($newInstance['sectionTitle']);
        $values['shortDesc'] = esc_attr($newInstance['shortDesc']);
        $values['link'] = $newInstance['link'];

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {
        global $text_domain;

        if(!$instance['status']){
            return;
        }

        extract($args);

        $sectionTitle = esc_attr($instance['sectionTitle']);
        $shortDesc = esc_attr($instance['shortDesc']);
        $link = $instance['link'];

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }
            $expanded = 'true';
            $collapse = ' collapse in';
            $class = '';
            ?>

            <!--begin section-accordion-->
            <section<?= $sectionId; ?> class="section-base light-section">
                <div class="container">
                    <div class="section-title-level-1">
                        <h1><?= $sectionTitle; ?></h1>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel-group accordion-md" id="accordion_<?= $args['widget_id'];?>" role="tablist" aria-multiselectable="true">
                            <?php if(count($dynamicFieldsArr)>0): ?>
                                <?php foreach($dynamicFieldsArr as $key =>$item): ?>
                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="heading_<?= $args['widget_id'];?>_<?= $key; ?>">
                                        <h4 class="panel-title">
                                            <a<?= $class; ?> role="button" data-toggle="collapse" data-parent="#accordion_<?= $args['widget_id'];?>" href="#collapse_<?= $args['widget_id'];?>_<?= $key; ?>" aria-expanded="<?= $expanded; ?>" aria-controls="collapse_<?= $args['widget_id'];?>_<?= $key; ?>">
                                                <?= $item['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $args['widget_id'];?>_<?= $key; ?>" class="panel-collapse<?= $collapse; ?>" role="tabpanel" aria-labelledby="heading_<?= $args['widget_id'];?>_<?= $key; ?>">
                                        <div class="panel-body">
                                            <?= $item['desc']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if($expanded == 'true'){
                                    $expanded = 'false';
                                }
                                if($collapse == ' collapse in'){
                                    $collapse = ' collapse';
                                }
                                if(!$class){
                                    $class = ' class="collapsed";';
                                }
                                ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="card-lg card-m-2">
                                <div class="card-img-top">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/question-icon-gold.png" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="card-text">
                                        <p><?= $shortDesc; ?></p>
                                    </div>
                                    <a href="<?= $link; ?>" class="card-button scroll"><span><?= __('Learn more', $text_domain) ?></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-accordion-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainFaqWidget");
});