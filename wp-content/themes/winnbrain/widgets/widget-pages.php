<?php
class WinnbrainPagesWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Pages-widget', __('Winnbrain Pages | Widget', 'winnbrain'),
           ['description' => __('Pages\' links', 'winnbrain')]);
    }

    public function form($instance) {
        $sectionTitle = '';
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [class="widefat datafield fieldTitle"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><input [class="widefat datafield linkOnly getLinksList"/] [title="Link"/] [name="link"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', 'winnbrain'), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['sectionTitle'] = $newInstance['sectionTitle'];

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $sectionTitle = $instance['sectionTitle'];

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            $itemID = null;
            if(strpos($_SERVER["REQUEST_URI"], '?')>-1){
                $urlParts = explode('&', substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], '?')+1));
                if(count($urlParts)>0){
                    foreach ($urlParts as $result){
                        $activeWidget = strrpos($result, 'actitem=');
                        if($activeWidget>-1){
                            $itemID = substr($result, $activeWidget+8);
                        }
                    }
                }
            }
            ?>

            <section<?= $sectionId; ?> class="services section-base section-list-item-1 light-section">
                <div class="inner-wrap">
                    <div class="container">
                        <?php if($sectionTitle): ?>
                        <div class="section-title">
                            <h2><?= $sectionTitle; ?></h2>
                        </div>
                        <?php endif; ?>
                        <div class="row">
                        <?php if(count($dynamicFieldsArr)>0): ?>
                            <div class="page-item-cont">
                                <div class="page-item-wrap">
                                    <?php foreach($dynamicFieldsArr as $item): ?>
                                    <?php
                                        $highLight = $item['item_id']===$itemID?' high-lighted':'';
                                    ?>
                                    <div class="page-item<?= $highLight; ?>">
                                        <?php
                                        if(@$item['link'] && @$item['title']){
                                            echo '<a href="'.$item['link'].'?actitem='.$item['item_id'].'" title="'.$item['title'].'">'.cutString($item['title'], $count=20).'</a>';
                                        }
                                        else{
                                            echo cutString(@$item['title'], $count=30);
                                        }
                                        ?>
                                    </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainPagesWidget");
});