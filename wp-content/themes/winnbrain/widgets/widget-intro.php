<?php
class WinnbrainIntroWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Intro-widget', __('Winnbrain Intro | Widget', $text_domain),
           ['description' => __('Main site information', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;

        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [type="text"/] [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield linkOnly getLinksList"/] [title="Link"/] [name="link"/] /></p>';
        $pattern .= '<p><checkbox [class="widefat datafield"/] [title="Open link in new window"/] [name="linkTarget"/] /></p>';
        $pattern .= '<p><media [data="multiple:false,type:image"/] [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', $text_domain), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {
        global $text_domain;

        if(!$instance['status']){
            return;
        }

        extract($args);

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="section-intro-slider dark-section">
                <div class="slider-intro">

                <?php if(count($dynamicFieldsArr)>0): ?>
                    <?php foreach($dynamicFieldsArr as $item): ?>

                    <div class="item">
                        <div class="img-box">
                            <?php if($item['img']): ?>
                                <img src="<?= $item['img']; ?>" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="overlay-box"></div>
                        <div class="item-content-box">
                            <?php if($item['title']): ?>
                            <div class="item-heading">
                                <h2><?= $item['title']; ?></h2>
                            </div>
                            <?php endif; ?>
                            <?php if($item['desc']): ?>
                            <div class="item-text">
                                <p><?= $item['desc']; ?></p>
                            </div>
                            <?php endif; ?>
                            <?php if($item['link']): ?>
                            <?php
                                $target = '';
                                if($item['linkTarget']){
                                    $target = ' target="_blank"';
                                }
                            ?>
                            <a<?= $target; ?> href="<?= $item['link']; ?>" class="main-button white-button"><span><?php _e('Learn more', $text_domain); ?></span></a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php endforeach; ?>
                <?php endif; ?>

                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainIntroWidget");
});