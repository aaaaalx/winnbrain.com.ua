<?php
class WinnbrainPartnersWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Partners-widget', __('Winnbrain Partners | Widget', 'winnbrain'),
           ['description' => __('Partners information', 'winnbrain')]);
    }

    public function form($instance) {
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><textarea [class="widefat datafield linkOnly getLinksList"/] [title="Link"/] [name="link"/] /></p>';
        $pattern .= '<p><checkbox [class="widefat datafield"/] [title="Open link in new window"/] [name="linkTarget"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', 'winnbrain'), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="section-base section-4-items-slider">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="slider-4-items">
                            <?php if(count($dynamicFieldsArr)>0): ?>
                                <?php foreach($dynamicFieldsArr as $img): ?>
                                <div class="item">
                                    <?php if($img['link']): ?>
                                    <?php
                                    $target = $img['linkTarget']?' target="_blank"':'';
                                    ?>
                                    <a href="<?= $img['link']; ?>"<?= $target; ?>>
                                    <?php endif; ?>
                                        <img src="<?= $img['img']; ?>" alt="">
                                    <?php if($img['link']): ?>
                                    </a>
                                    <?php endif; ?>
                                </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainPartnersWidget");
});