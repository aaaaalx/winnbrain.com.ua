<?php
class WinnbrainContactsWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Contacts-widget', __('Winnbrain Contacts | Widget', 'winnbrain'),
           ['description' => __('Main site contacts', 'winnbrain')]);
    }

    public function form($instance) {
        $sectionTitle = '';
        $sectionDesc = '';
        $address = '';
        $coordinates = '';
        $dynam_emails = '';
        $dynam_phones = '';
        $timetable = '';
        $website = '';
        $mapZoom = '';
        $mapTitle = '';
        $mapInfoWindowCont = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);
            $sectionDesc = esc_attr($instance['sectionDesc']);
            $address = esc_attr($instance['address']);
            $coordinates = esc_attr($instance['coordinates']);
            $dynam_emails = $instance['dynam_emails'];
            $dynam_phones = $instance['dynam_phones'];
            $timetable = $instance['timetable'];
            $website = $instance['website'];
            $mapZoom = esc_attr($instance['mapZoom']);
            $mapTitle = esc_attr($instance['mapTitle']);
            $mapInfoWindowCont = $instance['mapInfoWindowCont'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        $Id = $this->get_field_id('sectionDesc');
        $Name = $this->get_field_name('sectionDesc');
        echo '<label for="' . $Id . '">'.__( 'Section Description', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat" rows="3" id="' . $Id . '" name="' . $Name . '">' . $sectionDesc . '</textarea></p>';

        $Id = $this->get_field_id('address');
        $Name = $this->get_field_name('address');
        echo '<label for="' . $Id . '">'.__( 'Address', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat" rows="3" id="' . $Id . '" name="' . $Name . '">' . $address . '</textarea></p>';

        $Id = $this->get_field_id('coordinates');
        $Name = $this->get_field_name('coordinates');
        echo '<label for="' . $Id . '">'.__( 'Coordinates', 'winnbrain' ).': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $coordinates . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynam_emails');
        $Name = $this->get_field_name('dynam_emails');
        $pattern = '<p><input [class="widefat datafield"/] [title="Email"/] [name="email"/] /></p>';
        insertDynamicField($Id, __('Emails', 'winnbrain'), $dynam_emails, $pattern, $Name);
        echo '<hr>';

        $Id = $this->get_field_id('dynam_phones');
        $Name = $this->get_field_name('dynam_phones');
        $pattern = '<p><input [class="widefat datafield"/] [title="Phone"/] [name="phone"/] /></p>';
        insertDynamicField($Id, __('Phones', 'winnbrain'), $dynam_phones, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('timetable');
        $Name = $this->get_field_name('timetable');
        echo '<label for="' . $Id . '">'.__( 'Timetable', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat addEditor" rows="3" id="' . $Id . '" name="' . $Name . '">' . $timetable . '</textarea></p>';

        $Id = $this->get_field_id('website');
        $Name = $this->get_field_name('website');
        echo '<label for="' . $Id . '">'.__( 'Website', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat addEditor" rows="3" id="' . $Id . '" name="' . $Name . '">' . $website . '</textarea></p>';

        $Id = $this->get_field_id('mapZoom');
        $Name = $this->get_field_name('mapZoom');
        echo '<p><label for="' . $Id . '">' . __('Map Zoom', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="number" name="' . $Name . '" min="2" max="22" value="'.($mapZoom?$mapZoom:2).'"></p>';

        $Id = $this->get_field_id('mapTitle');
        $Name = $this->get_field_name('mapTitle');
        echo '<label for="' . $Id . '">'.__( 'Map Title', 'winnbrain' ).': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $mapTitle . '"></p>';

        $Id = $this->get_field_id('mapInfoWindowCont');
        $Name = $this->get_field_name('mapInfoWindowCont');
        echo '<label for="' . $Id . '">'.__( 'Map Info Window Content', 'winnbrain' ).': </label>';
        echo '<textarea class="widefat addEditor" rows="3" id="' . $Id . '" name="' . $Name . '">' . $mapInfoWindowCont . '</textarea></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['sectionTitle'] = $newInstance['sectionTitle'];
        $values['sectionDesc'] = $newInstance['sectionDesc'];
        $values['address'] = trim(htmlentities($newInstance['address']));
        $values['coordinates'] =  htmlentities($newInstance['coordinates']);

        if($values['address'] && !$values['coordinates'] || $newInstance != $oldInstance){
            $latNlang = getCoordinates($values['address']);
            if($latNlang){
                $values['coordinates'] = $latNlang;
            }
        }

        $values['dynam_emails'] = $newInstance['dynam_emails'];
        $values['dynam_phones'] = $newInstance['dynam_phones'];
        $values['timetable'] = $newInstance['timetable'];
        $values['website'] = $newInstance['website'];
        $values['mapZoom'] = $newInstance['mapZoom'];
        $values['mapTitle'] = htmlentities($newInstance['mapTitle']);
        $values['mapInfoWindowCont'] =  $newInstance['mapInfoWindowCont'];
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $sectionTitle = esc_attr($instance['sectionTitle']);
        $sectionDesc = esc_attr($instance['sectionDesc']);
        $coordinates = esc_attr($instance['coordinates']);
        $dynam_emails = $instance['dynam_emails'];
        $dynam_phones = $instance['dynam_phones'];
        $timetable = $instance['timetable'];
        $website = $instance['website'];
        $mapZoom = esc_attr($instance['mapZoom']);
        $mapTitle = esc_attr($instance['mapTitle']);
        $mapInfoWindowCont = $instance['mapInfoWindowCont'];
        $sectionId = esc_attr($instance['sectionId']);

        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        $coordinates = explode(',', $coordinates);
        $mapInfoWindowCont = preg_replace('/(\>)\s*(\<)/m', '$1$2', $mapInfoWindowCont);

        $dynamArr_emails = [];
        if($dynam_emails){
            $dynamArr_emails = json_decode($dynam_emails, true);
        }
        $dynamArr_phones = [];
        if($dynam_phones){
            $dynamArr_phones = json_decode($dynam_phones, true);
        }

        if(count($instance)>0) {

            ?>

            <section<?= $sectionId; ?> class="section-base section-contacts light-section">
                <div class="container">
                    <?php if($sectionTitle): ?>
                    <div class="section-title">
                        <h2><?= $sectionTitle; ?></h2>
                    </div>
                    <?php endif; ?>
                    <?php if($sectionDesc): ?>
                    <div class="section-description">
                        <p><?= $sectionDesc; ?></p>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="map-container">
                                <div id="map-canvas"></div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="contact-list-wrap">
                                <div class="row">
                                    <?php if(count($dynamArr_emails)>0): ?>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="email-box icon-box">
                                            <?php foreach($dynamArr_emails as $item): ?>
                                            <a href="mailto:<?= $item['email']; ?>"><?= $item['email']; ?></a>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if(count($dynamArr_phones)>0): ?>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="phone-box icon-box">
                                            <?php
                                            $str = '';
                                            $arrCount = count($dynamArr_phones);
                                            foreach($dynamArr_phones as $key => $item){
                                                ($key<$arrCount-1)?$str=',':$str='';
                                                echo '<a href="tel:'.str_replace(' ', '', $item['phone']).'">'.$item['phone'].$str.'</a>';
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($timetable): ?>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="mode-box icon-box">
                                            <?= $timetable; ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                    <?php if($website): ?>
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="web-box icon-box">
                                            <?= $website; ?>
                                        </div>
                                    </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <script>
                    //GOOGLE MAPS API
                    function initMap() {
                        if (document.getElementById('map-canvas')) {
                            var uluru = {
                                lat: <?= @$coordinates[0]?$coordinates[0]:0; ?>,
                                lng: <?= @$coordinates[1]?$coordinates[1]:0; ?>
                            };
                            var map = new google.maps.Map(document.getElementById('map-canvas'), {
                                <?php if($mapZoom): ?>
                                zoom: <?= $mapZoom; ?>,
                                <?php endif; ?>
                                center: uluru
                            });

                            var contentString = '<div id="content">' +
                                '<div id="siteNotice">' +
                                '</div>' +
                                '<h3 id="firstHeading" class="firstHeading"><?= $mapTitle; ?></h3>' +
                                '<div id="bodyContent">' +
                                '<?= addslashes(strip_tags($mapInfoWindowCont, '<em><p><b><strong><a><ol><ul><li>')); ?>' +
                                '</div>' +
                                '</div>';

                            var infowindow = new google.maps.InfoWindow({
                                content: contentString
                            });

                            var marker = new google.maps.Marker({
                                position: uluru,
                                <?php if($mapTitle): ?>
                                title:"<?= $mapTitle; ?>",
                                <?php endif; ?>
                                map: map

                            });

                            function mapDescription() {
                                if ($(window).width() > 768) {
                                    infowindow.open(map, marker);
                                }
                            }

                            mapDescription();
                            $(window).resize(mapDescription);
                        }
                    }
                </script>
                <?php
                $key = get_option('field_mapKey');
                wp_register_script('user_map_script', 'https://maps.googleapis.com/maps/api/js?key='.$key.'&callback=initMap', '', '', true);
                wp_enqueue_script('user_map_script');
                ?>

            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainContactsWidget");
});