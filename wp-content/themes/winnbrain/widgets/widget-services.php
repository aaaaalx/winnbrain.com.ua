<?php
class WinnbrainServicesWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Services-widget', __('Winnbrain Services | Widget', $text_domain),
           ['description' => __('Services information', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;

        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [type="text"/] [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield linkOnly getLinksList"/] [title="Link"/] [name="link"/] /></p>';
        $pattern .= '<p><checkbox [class="widefat datafield"/] [title="Open link in new window"/] [name="linkTarget"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', $text_domain), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        global $text_domain;

        if(!$instance['status']){
            return;
        }

        extract($args);

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="services section-base section-list-item-1 light-section">
                <div class="container">
                    <div class="row">
                    <?php if(count($dynamicFieldsArr)>0): ?>
                        <?php foreach($dynamicFieldsArr as $item): ?>
                        <div class="col-md-4 col-xs-12">
                            <div class="card card-0">
                                <div class="card-img-top">
                                    <?php
                                    $image = '';
                                    if($item['img']){
                                        $attachment_id = get_attachment_file_id($item['img']);
                                        if($attachment_id){
                                            $image = wp_get_attachment_image($attachment_id, 'thumbnail');
                                        }
                                    }
                                    ?>
                                    <?php if($image): ?>
                                        <?= $image; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3><?= $item['title']; ?></h3>
                                    </div>
                                    <div data-group="services" class="card-text same-height">
                                        <p><?= cutString($item['desc'], 300); ?></p>
                                    </div>
                                    <?php if($item['link']): ?>
                                    <?php
                                    $target = '';
                                    if($item['linkTarget']){
                                        $target = ' target="_blank"';
                                    }
                                    ?>
                                    <a<?= $target; ?> href="<?= $item['link']; ?>" class="card-button"><span><?= __('Learn more', $text_domain) ?></span></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainServicesWidget");
});