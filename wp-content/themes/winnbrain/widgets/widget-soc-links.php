<?php
class WinnbrainSocLinksWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('SocLinks-widget', __('Winnbrain Social Links | Widget', 'winnbrain'),
           ['description' => __('Social Links', 'winnbrain')]);
    }

    public function form($instance) {
        $dynamicFields = '';
        $status = '';

        if (!empty($instance)) {
            $dynamicFields = $instance['dynamicFields'];
            $status = esc_attr($instance['status']);
        }

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><input [class="widefat datafield"/] [title="Link"/] [name="link"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', 'winnbrain'), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);

        $dynamicFields = $instance['dynamicFields'];

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <div class="social-buttons-list closed">
            <?php if(count($dynamicFieldsArr)>0): ?>
                <?php foreach($dynamicFieldsArr as $item): ?>
                    <?php if($item['link']): ?>
                    <a href="<?= $item['link']; ?>" target="_blank">
                    <?php endif; ?>
                        <?php
                            $title = $item['title']?' '.$item['title']:'';
                        ?>
                        <div class="social-button-box<?= $title; ?>" style="background-image: url(<?= $item['img']; ?>);"></div>
                    <?php if($item['link']): ?>
                    </a>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            </div>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainSocLinksWidget");
});