<?php
class WinnbrainAdvantagesWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Advantages-widget', __('Winnbrain Advantages | Widget', 'winnbrain'),
           ['description' => __('Advantages information', 'winnbrain')]);
    }

    public function form($instance) {
        $sectionTitle = '';
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><textarea [class="widefat datafield fieldTitle"/] [title="Description"/] [name="desc"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', 'winnbrain'), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['sectionTitle'] = $newInstance['sectionTitle'];

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $sectionTitle = $instance['sectionTitle'];

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="section-base section-list-item-1 sli-v-1 dark-section" id="sli-slider">
                <div class="overlay-box"></div>
                <div class="inner-wrap">
                    <div class="container">
                        <?php if($sectionTitle): ?>
                        <div class="section-title">
                            <h2><?= $sectionTitle; ?></h2>
                        </div>
                        <?php endif; ?>
                        <div class="row">
                        <?php if(count($dynamicFieldsArr)>0): ?>
                            <?php foreach($dynamicFieldsArr as $item): ?>
                            <div class="col-md-4 col-sm-6">
                                <div class="card card-m">
                                    <div class="card-img-top">
                                        <?php
                                        $image = '';
                                        if($item['img']){
                                            $attachment_id = get_attachment_file_id($item['img']);
                                            if($attachment_id){
                                                $image = wp_get_attachment_image($attachment_id, 'thumbnail');
                                            }
                                        }
                                        ?>
                                        <?php if($image): ?>
                                            <?= $image; ?>
                                        <?php endif; ?>
                                    </div>
                                    <div class="card-body">
                                        <div data-group="advantages" class="card-text same-height">
                                            <p><?= wp_trim_words( $item['desc'], 40, '...' ); ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainAdvantagesWidget");
});