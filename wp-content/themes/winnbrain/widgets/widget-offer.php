<?php
class WinnbrainOfferWidget extends WP_Widget{
    public function __construct() {
        parent::__construct('Offer-widget', __('Winnbrain Offer | Widget', 'winnbrain'),
           ['description' => __('Offers information', 'winnbrain')]);
    }

    public function form($instance) {

        $formTitle = '';

        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $formTitle = esc_attr($instance['formTitle']);
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('formTitle');
        $Name = $this->get_field_name('formTitle');
        echo '<p><label for="' . $Id . '">' . __('Form title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $formTitle . '"></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['formTitle'] = $newInstance['formTitle'];
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);

        $formTitle = esc_attr($instance['formTitle']);

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        $form_id = $args['widget_id'].'-modalForm';

        if(count($instance)>0) {

            ?>

            <section<?= $sectionId; ?> class="section-base dark-section section-offer so-2">
                <div class="container">
                    <div class="row row-flex-y-center">
                        <div class="col-md-2 col-xs-12 center-y">
                            <div class="offer-img-left">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/smile-icon-down.png" alt="">
                            </div>
                        </div>
                        <div class="col-md-8 col-xs-12 center-y">
                            <div class="offer-box">
                                <div class="offer-title">
                                    <h2><?= __('Tired of problems with bookkeeping?', 'winnbrain') ?></h2>
                                </div>
                                <div class="offer-text">
                                    <p><?= __('Get advice from a WinnBrain Specialist', 'winnbrain') ?></p>
                                </div>
                                <button type="button" class="main-button white-button" data-toggle="modal" data-target="#<?= $form_id; ?>"><span><?= __('to get the consultation', 'winnbrain') ?></span></button>
                            </div>
                        </div>
                        <div class="col-md-2 col-xs-12 center-y">
                            <div class="offer-img-right">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/smile-icon-up.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>

                <!--begin modal form-popup-->
                <div class="modal-form-1 modal fade" id="<?= $form_id; ?>" tabindex="-1" role="dialog" aria-labelledby="form-popup-1">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-wrap">
                                    <div class="form-header">
                                        <h4><?= __('Submit your application', 'winnbrain') ?></h4>
                                    </div>
                                    <div class="form-description">
                                        <?= __('Leave your contact information and our specialist will contact you shortly', 'winnbrain') ?>
                                    </div>
                                    <form data-url="<?= admin_url('admin-ajax.php'); ?>" method="post" class="form form-1">
                                        <input type="hidden" data-form="form-2" class="input-name hidden-data" value="<?= $formTitle; ?>">
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control input-name username" name="username" required placeholder="<?= __('Your name', 'winnbrain') ?>">
                                            </div>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control input-phone phone" name="phone" required id="input-phone-3" placeholder="<?= __('Your phone', 'winnbrain') ?>">
                                            </div>
                                            <div class="col-xs-12">
                                                <input type="text" class="form-control input-email email" name="email" placeholder="<?= __('Your email', 'winnbrain') ?>">
                                            </div>
                                            <div class="col-xs-12">
                                                <button type="submit" class="main-button blue-button"><span><?= __('Submit', 'winnbrain') ?></span></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end modal form-popup-->

            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainOfferWidget");
});