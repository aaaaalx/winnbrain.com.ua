<?php
class WinnbrainTestimonialsWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Testimonials-widget', __('Winnbrain Testimonials | Widget', $text_domain),
           ['description' => __('Testimonials information', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;

        $sectionTitle = '';
        $postsCount = '';
        $orderBy = '';
        $order = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = $instance['sectionTitle'];
            $postsCount = $instance['postsCount'];
            $orderBy = $instance['orderBy'];
            $order = $instance['order'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        $Id = $this->get_field_id('postsCount');
        $Name = $this->get_field_name('postsCount');
        echo '<p><label for="' . $Id . '">' . __('Posts count', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="number" min="1" max="10" name="' . $Name . '" value="' . $postsCount . '"></p>';

        $listOrderBy = getSortOrderBy();
        $Id = $this->get_field_id('orderBy');
        $Name = $this->get_field_name('orderBy');
        echo '<p><label for="' . $Id . '">' . __('Order by', $text_domain) . ': </label>';
        echo '<select class="widefat" id="'.$Id.'" name="'.$Name.'">';
        if(count($listOrderBy)>0){
            foreach ($listOrderBy as $key => $item){
                $selected = '';
                if($orderBy == $key){
                    $selected = ' selected';
                }
                echo '<option value="'.$key.'"'.$selected.'>'.$item.'</option>';
            }
        }
        echo '</select>';

        $listOrder = getSortOrder();
        $Id = $this->get_field_id('order');
        $Name = $this->get_field_name('order');
        echo '<p><label for="' . $Id . '">' . __('Order', $text_domain) . ': </label>';
        echo '<select class="widefat" id="'.$Id.'" name="'.$Name.'">';
        if(count($listOrder)>0){
            foreach ($listOrder as $key => $item){
                $selected = '';
                if($order == $key){
                    $selected = ' selected';
                }
                echo '<option value="'.$key.'"'.$selected.'>'.$item.'</option>';
            }
        }
        echo '</select>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['sectionTitle'] = $newInstance['sectionTitle'];
        $values['postsCount'] = $newInstance['postsCount']>0?$newInstance['postsCount']:1;
        $values['orderBy'] = $newInstance['orderBy'];
        $values['order'] = $newInstance['order'];
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $sectionTitle = $instance['sectionTitle'];

        $postsCount = $instance['postsCount'];
        $orderBy = $instance['orderBy'];
        $order = $instance['order'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $args = [
                'posts_per_page'   => $postsCount,
                'offset'           => 0,
                'category'         => '',
                'category_name'    => '',
                'orderby'          => $orderBy,
                'order'            => $order,
                'include'          => '',
                'exclude'          => '',
                'meta_key'         => '',
                'meta_value'       => '',
                'post_type'        => 'testimonials',
                'post_mime_type'   => '',
                'post_parent'      => '',
                'author'	   => '',
                'author_name'	   => '',
                'post_status'      => 'publish',
                'suppress_filters' => true
            ];
            if($orderBy == 'meta_value'){
                $args['orderby'] = 'meta_value_num';
                $args['meta_key'] = 'meta_value_num';
            }
            $posts_array = get_posts($args);

            ?>

            <section<?= $sectionId; ?> class="section-base light-section section-slider">
                <div class="container">
                    <?php if($sectionTitle): ?>
                    <div class="section-title">
                        <h2><?= $sectionTitle; ?></h2>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="slider-1-item media-presentation">
                            <?php if(count($posts_array)>0): ?>
                                <?php foreach($posts_array as $post): ?>
                                <?php
                                $title = get_the_title($post->ID);
                                $content = get_post_field('post_content', $post->ID);
                                $short_desc = get_post_meta($post->ID, '_testimonials_short_desc_value_key', true);
                                $image = get_the_post_thumbnail_url($post->ID, 'large');
                                $image = $image?$image:get_stylesheet_directory_uri().'/img/No-photo_228x305.jpg';
                                ?>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-3 hidden-xs">
                                            <?php if($image): ?>
                                            <div class="img-box">
                                                <img src="<?= $image; ?>" alt="">
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="item-text">
                                                <p><?= wp_trim_words( strip_shortcodes(strip_tags($content)), 45, '...' ); ?></p>
                                            </div>
                                            <div class="item-info-box">
                                                <?php if(strip_tags($title)): ?>
                                                <h4><?= strip_tags($title); ?></h4>
                                                <?php endif; ?>
                                                <?php if($short_desc): ?>
                                                <p><?= wp_trim_words( $short_desc, 18, '...' ); ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainTestimonialsWidget");
});