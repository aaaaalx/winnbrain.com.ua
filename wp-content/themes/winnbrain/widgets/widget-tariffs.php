<?php
class WinnbrainTariffsWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Tariffs-widget', __('Winnbrain Tariffs | Widget', $text_domain),
           ['description' => __('Tariffs', $text_domain)]);
    }

    public function getOptionTypesList(){
        global $text_domain;
        return [
                1 => __('Checkbox', $text_domain),
                2 => __('Number', $text_domain)
        ];
    }

    public function form($instance) {
        global $text_domain;

        $sectionTitle = '';

        $dynamicPackages = '';
        $dynamicOptions = '';
        $dynamicAdditionalOptions = '';
        $dynamicCommonValues = '';

        $link = '';
        $showTotalPrices = '';

        $offsetTop = '';
        $offsetBottom = '';

        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);

            $dynamicPackages = $instance['dynamicPackages'];
            $dynamicOptions = $instance['dynamicOptions'];
            $dynamicAdditionalOptions = $instance['dynamicAdditionalOptions'];
            $dynamicCommonValues = $instance['dynamicCommonValues'];

            $link = $instance['link'];
            $showTotalPrices = $instance['showTotalPrices'];

            $offsetTop = $instance['offsetTop'];
            $offsetBottom = $instance['offsetBottom'];

            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicPackages');
        $Name = $this->get_field_name('dynamicPackages');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><multiselect [list="func:getItemsFromWidgetDynamicFields('.$this->id.', dynamicOptions, true)"/] [class="widefat datafield editableSelect"/] [title="Services"/] [name="options"/] /></p>';
        $pattern .= '<p><input [class="widefat datafield"/] [title="Price"/] [name="price"/] /></p>';
        $pattern .= '<input [type="hidden"/] [class="datafield htmlentities"/] [name="costs"/] />';
        insertDynamicField($Id, __('Packages', $text_domain), $dynamicPackages, $pattern, $Name);

        //checkbox "Show total prices"
        $Id = $this->get_field_id('showTotalPrices');
        $Name = $this->get_field_name('showTotalPrices');
        echo '<p><label for="' . $Id . '">' . __('Show prices', $text_domain) . ': </label>';
        $checked = '';
        if($showTotalPrices == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

        echo '<hr>';

        $Id = $this->get_field_id('dynamicOptions');
        $Name = $this->get_field_name('dynamicOptions');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        insertDynamicField($Id, __('Services', $text_domain), $dynamicOptions, $pattern, $Name);
        echo '<hr>';

        $Id = $this->get_field_id('dynamicAdditionalOptions');
        $Name = $this->get_field_name('dynamicAdditionalOptions');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><input [class="widefat datafield"/] [title="Value"/] [name="value"/] /></p>';
        insertDynamicField($Id, __('Additional services', $text_domain), $dynamicAdditionalOptions, $pattern, $Name);
        echo '<hr>';

        $Id = $this->get_field_id('dynamicCommonValues');
        $Name = $this->get_field_name('dynamicCommonValues');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        insertDynamicField($Id, __('Common values', $text_domain), $dynamicCommonValues, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('offsetTop');
        $Name = $this->get_field_name('offsetTop');
        echo '<p><label for="' . $Id . '">' . __('Top widget offset', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="number" name="' . $Name . '" min="0" max="100" value="'.$offsetTop.'"></p>';

        $Id = $this->get_field_id('offsetBottom');
        $Name = $this->get_field_name('offsetBottom');
        echo '<p><label for="' . $Id . '">' . __('Bottom widget offset', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="number" name="' . $Name . '" min="0" max="100" value="'.$offsetBottom.'"></p>';

        $Id = $this->get_field_id('link');
        $Name = $this->get_field_name('link');
        echo '<p><label for="' . $Id . '">' . __('Link', $text_domain) . ': </label>';
        echo '<input class="widefat linkOnly getLinksList" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $link . '"></p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';

        //popup
        $dynamicCommonValuesStr = '';
            if($dynamicCommonValues){
                $dynamicCommonValuesArr = json_decode($dynamicCommonValues, true);
                if(is_array($dynamicCommonValuesArr) && count($dynamicCommonValuesArr)>0){
                    foreach ($dynamicCommonValuesArr as $k => $item){
                        $dynamicCommonValuesArr[$item['item_id']] = $item['title'];
                        $dynamicCommonValuesStr .= '<option value="'.$item['item_id'].'">'.$item['title'].'</option>';
                        unset($dynamicCommonValuesArr[$k]);
                    }
                }
            }

        echo '<div style="display: none;" id="'.$this->id.'-popup-selectEdit">';
            echo '<div class="popup-inner">';

            echo '<h3>'.__('Editing', $text_domain).': <span id="currentEditName"></span></h3>';

            echo '<div class="form-group">';
                echo '<p>'.__('Default value', $text_domain).': ';
                echo '<input class="selectEditRadio" type="radio" class="form-control" name="data-type" value="0"><br>';
                echo __('Data from field "Value"', $text_domain).': ';
                echo '<input class="selectEditRadio" type="radio" class="form-control" name="data-type" value="1"><br>';
                echo __('Common values', $text_domain).': ';
                echo '<input class="selectEditRadio" type="radio" class="form-control" name="data-type" value="2"></p>';
            echo '</div>';

            echo '<div class="form-group">';
                echo '<p>'.__('Value', $text_domain).': ';
                echo '<input type="text" class="form-control" name="data-value"></p>';
            echo '</div>';

            echo '<div class="form-group">';
                echo '<p>'.__('Common values', $text_domain).': ';
                echo '<select class="form-control" name="data-common"><option value="">'.__('Select value', $text_domain).'</option>'.$dynamicCommonValuesStr.'</select></p>';
            echo '</div>';

        echo '<div class="form-group">';
        echo '<p>'.__('Highlight value', $text_domain).': ';
        echo '<input class="form-control" name="data-highlight" type="checkbox" value="1"></p>';
        echo '</div>';

            echo '<br><br><button class="button button-primary editPopupButton">'.__('Edit', $text_domain).'</button>';

            echo '</div>';
        echo '</div>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['sectionTitle'] = esc_attr($newInstance['sectionTitle']);

        $values['dynamicPackages'] = $newInstance['dynamicPackages'];
        $values['dynamicOptions'] = $newInstance['dynamicOptions'];
        $values['dynamicAdditionalOptions'] = $newInstance['dynamicAdditionalOptions'];
        $values['dynamicCommonValues'] = $newInstance['dynamicCommonValues'];

        $values['link'] = htmlentities($newInstance['link']);
        $values['showTotalPrices'] = htmlentities($newInstance['showTotalPrices']);

        $values['offsetTop'] = $newInstance['offsetTop'];
        $values['offsetBottom'] = $newInstance['offsetBottom'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {
        global $text_domain;

        if(!$instance['status']){
            return;
        }

        extract($args);

        $sectionTitle = esc_attr($instance['sectionTitle']);

        $dynamicPackages = $instance['dynamicPackages'];
        $dynamicOptions = $instance['dynamicOptions'];
        $dynamicAdditionalOptions = $instance['dynamicAdditionalOptions'];
        $dynamicCommonValues = $instance['dynamicCommonValues'];

        $calcLink = esc_attr($instance['link']);
        $showTotalPrices = $instance['showTotalPrices'];

        $offsetTop = esc_attr($instance['offsetTop']);
        $offsetBottom = esc_attr($instance['offsetBottom']);
        $offset = '';

        if(is_numeric($offsetTop) && is_numeric($offsetBottom)){
            $offset = ' style="padding-top:'.$offsetTop.'px;padding-bottom:'.$offsetBottom.'px;"';
        }
        elseif(is_numeric($offsetBottom)){
            $offset = ' style="padding-bottom:'.$offsetBottom.'px;"';
        }
        elseif(is_numeric($offsetTop)){
            $offset = ' style="padding-top:'.$offsetTop.'px;"';
        }

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicPackagesArr = [];
            $dynamicOptionsArr = [];
            $dynamicAdditionalOptionsArr = [];
            $dynamicCommonValuesArr = [];
            if($dynamicPackages){
                $dynamicPackagesArr = json_decode($dynamicPackages, true);
                if($dynamicOptions){
                    $dynamicOptionsArr = json_decode($dynamicOptions, true);
                    if(is_array($dynamicOptionsArr) && count($dynamicOptionsArr)>0){
                        foreach ($dynamicOptionsArr as $k => $item){
                            $dynamicOptionsArr[$item['item_id']] = $item['title'];
                            unset($dynamicOptionsArr[$k]);
                        }
                    }
                    if($dynamicCommonValues){
                        $dynamicCommonValuesArr = json_decode($dynamicCommonValues, true);
                        if(is_array($dynamicCommonValuesArr) && count($dynamicCommonValuesArr)>0){
                            foreach ($dynamicCommonValuesArr as $k => $item){
                                $dynamicCommonValuesArr[$item['item_id']] = $item['title'];
                                unset($dynamicCommonValuesArr[$k]);
                            }
                        }
                    }
                }
            }
            if($dynamicAdditionalOptions){
                $dynamicAdditionalOptionsArr = json_decode($dynamicAdditionalOptions, true);
            }

            ?>

            <!--begin section-media-object-->
            <section<?= $sectionId; ?> class="section-base section-table"<?= $offset; ?>>
                <div class="container">
                    <div class="section-title-level-2">
                        <h1 class="tableTitle"><?= $sectionTitle; ?></h1>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <?php if(is_array($dynamicPackagesArr) && count($dynamicPackagesArr)>0): ?>
                            <div class="table-responsive">
                                <?php
                                if($showTotalPrices){
                                    $tfootTemplate = '<tfoot><tr><td class="rate-choice-noborder"></td>[buttons_packages]</tr>'/*<tr><td class="rate-choice-noborder"></td>[prices]</tr>*/.'<tr><td class="rate-choice-buttom rate-choice-noborder"></td>[buttons]</tr></tfoot>';
                                }
                                else{
                                    $tfootTemplate = '<tfoot><tr><td class="rate-choice-noborder"></td>[buttons_packages]</tr><tr><td class="rate-choice-buttom rate-choice-noborder"></td>[buttons]</tr></tfoot>';
                                }
                                $outputString = [
                                    'thead' => [
                                        'template' => '<thead><tr><th>'.__('Service', $text_domain).'</th>[packages]</tr></thead>',
                                        'rows' => [
                                            'packages' => ''
                                        ]
                                    ],
                                    'tfoot' => [
                                        'template' => $tfootTemplate,
                                        'rows' => [
                                            //'prices' => '',
                                            'buttons' => '',
                                            'buttons_packages' => '',
                                        ]
                                    ],
                                    'tbody' => [
                                        'template' => '<tbody>[options]</tbody>',
                                        'rows' => [
                                            'options' => ''
                                        ]
                                    ],
                                ];

                                $costsArr = [];
                                foreach ($dynamicPackagesArr as $k => $package){
                                    $outputString['thead']['rows']['packages'] .= '<th><span id="rate-'.(++$k).'">'.$package['title'].'</span></th>';
                                    $price = '';
                                    if($showTotalPrices){
                                        //$outputString['tfoot']['rows']['prices'] .= '<td>'.$package['price'].'</td>';
                                        $price = $package['price'].'<br>';
                                    }
                                    $outputString['tfoot']['rows']['buttons'] .= '<td class="rate-choice-buttom">'.$price.'<button type="button" class="btn main-button gold-button rate-choice" data-toggle="modal" data-target="#form-popup-1" value=""> <span>'.__('To order', $text_domain).'</span></button></td>';
                                    $outputString['tfoot']['rows']['buttons_packages'] .= '<td class="rate-choice-packages">'.$package['title'].'</td>';
                                    $costsArr[$package['item_id']] = (array)json_decode(@$package['costs'], true);
                                }

                                if(is_array($dynamicOptionsArr) && count($dynamicOptionsArr)>0) {
                                    $unitedCells = [];
                                    $numCont = [];

                                    foreach ($dynamicOptionsArr as $k => $option) {
                                        $outputString['tbody']['rows']['options'] .= '<tr>';
                                        $outputString['tbody']['rows']['options'] .= '<td>' . $option . '</td>';
                                        $num = 0;
                                        foreach ($dynamicPackagesArr as $k2 => $package) {
                                            $costs = @$costsArr[$package['item_id']];
                                            $costs[$k]['dataType'] = isset($costs[$k]['dataType']) ? $costs[$k]['dataType'] : 0;
                                            if (isset($costs[$k]['dataType']) && (int)$costs[$k]['dataType'] !== 2 ||
                                                is_array($package['options']) && !in_array($k, $package['options'])
                                            ) {
                                                $num += 1;
                                            }
                                            if (is_array($package['options']) && in_array($k, $package['options'])) {
                                                if (isset($costs[$k])) {
                                                    if ((int)$costs[$k]['dataType'] === 0) {
                                                        $unitedCells[$k][$package['item_id']] = '<td><span class="check-wrap checked"></span></td>';
                                                    } elseif ((int)$costs[$k]['dataType'] === 1) {
                                                        if($costs[$k]['dataHighlight'] == 1){
                                                            $value = '<strong>'.$costs[$k]['dataValue'].'</strong>';
                                                        }
                                                        else{
                                                            $value = $costs[$k]['dataValue'];
                                                        }
                                                        $unitedCells[$k][$package['item_id']] = '<td><span class="check-wrap-value">'.$value.'</span></td>';
                                                    } elseif ((int)$costs[$k]['dataType'] === 2) {
                                                        if (!isset($numCont[$k][$num][$costs[$k]['dataCommon']]) || @$numCont[$k]['dataCommon'][$num] != $costs[$k]['dataCommon']) {
                                                            $numCont[$k]['dataCommon'][$num] = $costs[$k]['dataCommon'];
                                                            $numCont[$k][$num][$costs[$k]['dataCommon']] = 1;
                                                            $link = &$numCont[$k][$num][$costs[$k]['dataCommon']];
                                                            $numCont[$k]['highlight'][$num][$costs[$k]['dataCommon']] = @$numCont[$k]['highlight'][$num][$costs[$k]['dataCommon']]?true:false;
                                                            if($costs[$k]['dataHighlight'] == 1){
                                                                $numCont[$k]['highlight'][$num][$costs[$k]['dataCommon']] = true;
                                                            }
                                                            $value = @$dynamicCommonValuesArr[$costs[$k]['dataCommon']];
                                                            $highlight = &$numCont[$k]['highlight'][$num][$costs[$k]['dataCommon']];
                                                            $number = function () use (&$link, &$highlight, $value) {
                                                                if($highlight){
                                                                    $value = '<strong>'.$value.'</strong>';
                                                                }
                                                                return '<td colspan="' . $link . '"><span class="check-wrap-value">'.$value.'</span></td>';
                                                            };
                                                            $unitedCells[$k][$package['item_id']] = $number;
                                                        } else {
                                                            $numCont[$k][$num][$costs[$k]['dataCommon']] += 1;
                                                            if($costs[$k]['dataHighlight'] == 1){
                                                                $numCont[$k]['highlight'][$num][$costs[$k]['dataCommon']] = true;
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $unitedCells[$k][$package['item_id']] = '<td><span class="check-wrap checked"></span></td>';
                                                }

                                            } else {
                                                $unitedCells[$k][$package['item_id']] = '<td><span class="check-wrap unchecked"></span></td>';
                                            }
                                        }

                                        if(is_array($unitedCells[$k]) && count($unitedCells[$k])>0){
                                            foreach ($unitedCells[$k] as $cell) {
                                                if (is_object($cell)) {
                                                    $outputString['tbody']['rows']['options'] .= $cell();
                                                } else {
                                                    $outputString['tbody']['rows']['options'] .= $cell;
                                                }
                                            }
                                        }

                                        $outputString['tbody']['rows']['options'] .= '</tr>';
                                    }

                                }

                                $template_thead = $outputString['thead']['template'];
                                foreach ($outputString['thead']['rows'] as $k => $item){
                                    $template_thead = str_replace('['.$k.']',$item,$template_thead);

                                }
                                $template_tfoot = $outputString['tfoot']['template'];
                                foreach ($outputString['tfoot']['rows'] as $k => $item){
                                    $template_tfoot = str_replace('['.$k.']',$item,$template_tfoot);

                                }
                                $template_tbody = $outputString['tbody']['template'];
                                foreach ($outputString['tbody']['rows'] as $k => $item){
                                    $template_tbody = str_replace('['.$k.']',$item,$template_tbody);
                                }

                                $template = '<table class="table table-lg">'.$template_thead.$template_tfoot.$template_tbody.'</table>';

                                echo $template;
                                ?>
                        </div>
                        <?php endif; ?>
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                <?php if(is_array($dynamicAdditionalOptionsArr) && count($dynamicAdditionalOptionsArr)>0): ?>
                                <table class="table table-md table-bordered">
                                    <thead>
                                    <tr>
                                        <th colspan="2"><?= __('Additional services', $text_domain); ?>:</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach($dynamicAdditionalOptionsArr as $item){
                                        echo '<tr><td>'.$item['title'].'</td><td>'.$item['value'].'</td></tr>';
                                    }
                                    ?>
                                    </tbody>
                                </table>
                                <?php endif; ?>

                            </div>
                        </div>

                        <?php if($calcLink): ?>
                        <div class="col-xs-12">
                            <div class="card card-m-2">
                                <div class="card-img-top">
                                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/icons/calculator-icon-gold.png" alt="">
                                </div>
                                <div class="card-body">
                                    <div class="card-title">
                                        <h3><?= __('Costs calculator', $text_domain); ?></h3>
                                    </div>
                                    <div class="card-text">
                                        <p><?= __('Do you want to calculate the cost of services in more detail? Use our calculator!', $text_domain); ?></p>
                                    </div>
                                    <a href="<?= $calcLink; ?>" class="card-button"><span><?= __('Learn more', $text_domain); ?></span></a>
                                </div>
                            </div>
                        </div>
                        <?php endif; ?>

                    </div>
                </div>


            </section>
            <!--end section-media-object-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainTariffsWidget");
});