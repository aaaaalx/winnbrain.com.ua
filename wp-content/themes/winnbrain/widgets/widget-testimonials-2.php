<?php
class WinnbrainTestimonials2Widget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Testimonials-2-widget', __('Winnbrain Testimonials 2 | Widget', $text_domain),
           ['description' => __('Testimonials 2: information', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;
        $sectionTitle = '';
        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = $instance['sectionTitle'];
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p>';
        $pattern .= '<p><textarea [class="widefat datafield"/] [title="Short Description"/] [name="short_desc"/] /></p>';
        $pattern .= '<p><media [title="Image"/] [name="img"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', $text_domain), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['sectionTitle'] = $newInstance['sectionTitle'];

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $sectionTitle = $instance['sectionTitle'];

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }

            ?>

            <section<?= $sectionId; ?> class="section-base light-section section-slider">
                <div class="container">
                    <?php if($sectionTitle): ?>
                    <div class="section-title">
                        <h2><?= $sectionTitle; ?></h2>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="slider-1-item media-presentation">
                            <?php if(count($dynamicFieldsArr)>0): ?>
                                <?php foreach($dynamicFieldsArr as $item): ?>
                                <?php
                                    $item['img'] = $item['img']?$item['img']:get_stylesheet_directory_uri().'/img/No-photo_228x305.jpg';
                                ?>
                                <div class="item">
                                    <div class="row">
                                        <div class="col-sm-3 hidden-xs">
                                            <?php if($item['img']): ?>
                                            <div class="img-box">
                                                <img src="<?= $item['img']; ?>" alt="">
                                            </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-sm-8 col-xs-12">
                                            <div class="item-text">
                                                <p><?= wp_trim_words( $item['desc'], 55, '...' ); ?></p>
                                            </div>
                                            <div class="item-info-box">
                                                <?php if($item['title']): ?>
                                                <h4><?= $item['title']; ?></h4>
                                                <?php endif; ?>
                                                <?php if($item['short_desc']): ?>
                                                <p><?= wp_trim_words( $item['short_desc'], 18, '...' ); ?></p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainTestimonials2Widget");
});