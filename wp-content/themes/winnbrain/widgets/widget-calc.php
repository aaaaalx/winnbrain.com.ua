<?php
class WinnbrainCalcWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Calc-widget', __('Winnbrain Calculator | Widget', $text_domain),
           ['description' => __('Calculator', $text_domain)]);
    }

    public function getOptionTypesList(){
        return [
                1 => __('Checkbox', $text_domain),
                2 => __('Number', $text_domain)
        ];
    }

    public function form($instance) {
        global $text_domain;

        $sectionTitle = '';
        $shortDesc = '';

        $dynamicServices = '';
        $dynamicTypes = '';
        $dynamicOptions = '';

        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $sectionTitle = esc_attr($instance['sectionTitle']);
            $shortDesc = esc_attr($instance['shortDesc']);

            $dynamicServices = $instance['dynamicServices'];
            $dynamicTypes = $instance['dynamicTypes'];
            $dynamicOptions = $instance['dynamicOptions'];

            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('sectionTitle');
        $Name = $this->get_field_name('sectionTitle');
        echo '<p><label for="' . $Id . '">' . __('Section Title', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionTitle . '"></p>';

        $Id = $this->get_field_id('shortDesc');
        $Name = $this->get_field_name('shortDesc');
        echo '<label for="' . $Id . '">'.__( 'Description', $text_domain ).': </label>';
        echo '<textarea class="widefat" rows="3" id="' . $Id . '" name="' . $Name . '">' . $shortDesc . '</textarea></p>';

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicServices');
        $Name = $this->get_field_name('dynamicServices');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><multiselect [list="func:getItemsFromWidgetDynamicFields('.$this->id.', dynamicTypes, true)"/] [class="widefat datafield"/] [title="Types"/] [name="types"/] /></p>';
        insertDynamicField($Id, __('Services', $text_domain), $dynamicServices, $pattern, $Name);
        echo '<hr>';

        $Id = $this->get_field_id('dynamicTypes');
        $Name = $this->get_field_name('dynamicTypes');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><multiselect [list="func:getItemsFromWidgetDynamicFields('.$this->id.', dynamicOptions, true)"/] [class="widefat datafield"/] [title="Options"/] [name="options"/] /></p>';
        insertDynamicField($Id, __('Services Types', $text_domain), $dynamicTypes, $pattern, $Name);
        echo '<hr>';

        $Id = $this->get_field_id('dynamicOptions');
        $Name = $this->get_field_name('dynamicOptions');
        $pattern = '<p><input [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';

        $pattern .= '<div class="hiddenDataWrap">';

        $list = json_encode($this->getOptionTypesList(), true);
        $pattern .= '<p><select [list="json:'.$list.'"/] [class="widefat datafield showHiddenData"/] [title="Type"/] [name="type"/] /></p>';

        $pattern .= '<div data-id="1" class="hiddenData">';
        $pattern .= '<p><radio [data="checked"/] [class="widefat datafield"/] [title="Checked"/] [name="checked"/] [value="on"/] /></p>';
        $pattern .= '<p><radio [class="widefat datafield"/] [title="Unchecked"/] [name="checked"/] [value="off"/] /></p>';
        $pattern .= '</div>';

        $pattern .= '<div data-id="2" class="hiddenData">';
        $pattern .= '<p><input [class="widefat datafield"/] [title="Value"/] [name="value"/] [type="number"/] /></p>';
        $pattern .= '</div>';

        $pattern .= '<p><checkbox [class="widefat datafield"/] [title="Inactive"/] [name="inactive"/] [value="inactive"/] /></p>';

        $pattern .= '</div>';

        $pattern .= '<p><input [class="widefat datafield"/] [title="Amount"/] [name="amount"/] [type="text"/] /></p>';
        insertDynamicField($Id, __('Services Types Options', $text_domain), $dynamicOptions, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['sectionTitle'] = esc_attr($newInstance['sectionTitle']);
        $values['shortDesc'] = esc_attr($newInstance['shortDesc']);

        $values['dynamicServices'] = $newInstance['dynamicServices'];
        $values['dynamicTypes'] = $newInstance['dynamicTypes'];
        $values['dynamicOptions'] = $newInstance['dynamicOptions'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {
        global $text_domain;

        if(!$instance['status']){
            return;
        }

        extract($args);

        $sectionTitle = esc_attr($instance['sectionTitle']);
        $shortDesc = esc_attr($instance['shortDesc']);

        $dynamicServices = $instance['dynamicServices'];
        $dynamicTypes = $instance['dynamicTypes'];
        $dynamicOptions = $instance['dynamicOptions'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            ?>

            <!--begin section-calculator-->
            <section class="section-base section-calcilator">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="section-title-level-1">
                                <h1>Калькулятор стоимости</h1>
                            </div>
                            <div class="calc-wrap">
                                <select data-url="<?= admin_url('admin-ajax.php'); ?>" data-target="get_types" class="aimedSelectList get_services calc-select">
                                    <option value="">Выберите услугу</option>
                                    <option value="1">Бухгалтерские услуги</option>
                                    <option value="2">Юридические услуги</option>
                                    <option value="3">Кадровый учет</option>
                                </select>
                                <select data-url="<?= admin_url('admin-ajax.php'); ?>" data-target="get_options" class="aimedSelectList get_types calc-select">
                                    <option value="">Выберите тип услуг</option>
                                </select>
                                <div class="form-description">
                                    <p>Выберите из списка интересующие вас услуги, или введите значения там, где это необходимо.</p>
                                </div>

                                <div class="get_options">
                                    <!--<form action="<?= admin_url('admin-ajax.php'); ?>" method="post" class="form-calc">-->
                                    <form action="/alert" method="post" class="form-calc">

                                        <div class="form-item-group">

                                            <div class="item">
                                                <div class="item-title">
                                                    1 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                    1 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                    1 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                </div>
                                                <div class="item-choice">
                                                    <input class="item_field item-checkbox disp-center" name="calc_item[1]" type="checkbox" value="1">
                                                </div>
                                                <div class="item-price">
                                                    <span class="item_amount disp-center">+<span class="value">1800</span> грн.</span>
                                                </div>
                                            </div>

                                            <div class="item">
                                                <div class="item-title">
                                                    2 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                    2 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                </div>
                                                <div class="item-choice">
                                                    <input class="item_field item-checkbox disp-center" name="calc_item[2]" type="checkbox" value="1" disabled="" checked="">
                                                </div>
                                                <div class="item-price">
                                                    <span class="item_amount disp-center">+<span class="value">1800</span> грн.</span>
                                                </div>
                                            </div>
                                            <div class="item">
                                                <div class="item-title">
                                                    3 Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                </div>
                                                <div class="item-choice">
                                                    <div class="item-counter-group disp-center">
                                                        <span class="input-number-decrement">–</span>
                                                        <input class="item_field  item-counter" name="calc_item[3]" type="number" data-value="0" min="0" max="100" step="1" value="0">
                                                        <span class="input-number-increment">+</span>
                                                    </div>
                                                </div>
                                                <div class="item-price">
                                                    <span class="item_amount disp-center">+<span class="value">20</span> грн.</span>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="total_amount">
                                            <div class="item-title">
                                                Общая стоимость
                                            </div>
                                            <div class="item-price">
                                                <span class="amount"><span class="value">1700</span> грн.</span>
                                            </div>
                                        </div>

                                        <button name="calc_from" value="1" class="submit_form main-button gold-button" type="submit">Заказать</button>
                                    </form>
                                    <div class="total-price-label" id="label-scroll">	<span class="amount"><span class="value">100000</span> грн.</span></div>
                                </div>

                            </div>
                        </div>
                    </div>
            </section>
            <!--end section-calculator-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainCalcWidget");
});