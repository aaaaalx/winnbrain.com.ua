<?php
class WinnbrainInfoWidget extends WP_Widget{
    public function __construct() {
        global $text_domain;
        parent::__construct('Info-widget', __('Winnbrain Info | Widget', $text_domain),
           ['description' => __('Information spoiler', $text_domain)]);
    }

    public function form($instance) {
        global $text_domain;

        $dynamicFields = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $dynamicFields = $instance['dynamicFields'];
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        /*dynamic fields*/

        $Id = $this->get_field_id('dynamicFields');
        $Name = $this->get_field_name('dynamicFields');
        $pattern = '<p><input [type="text"/] [class="widefat datafield"/] [title="Title"/] [name="title"/] /></p>';
        $pattern .= '<p><editor [class="widefat datafield"/] [title="Description"/] [name="desc"/] /></p>';
        insertDynamicField($Id, __('Dynamic Fields', $text_domain), $dynamicFields, $pattern, $Name);
        echo '<hr>';

        /*dynamic fields end*/

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', $text_domain) . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', $text_domain) . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();

        $values['dynamicFields'] = $newInstance['dynamicFields'];

        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);

        $dynamicFields = $instance['dynamicFields'];

        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if(count($instance)>0) {

            $dynamicFieldsArr = [];
            if($dynamicFields){
                $dynamicFieldsArr = json_decode($dynamicFields, true);
            }
            $expanded = 'true';
            $collapse = ' collapse';
            ?>

            <!--begin section-acсordion-->
            <section<?= $sectionId; ?> class="section-accordion grey-bg light-section">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="panel-group accordion-lg" id="accordion_<?= $args['widget_id'];?>" role="tablist" aria-multiselectable="true">
                            <?php if(count($dynamicFieldsArr)>0): ?>
                                <?php foreach($dynamicFieldsArr as $key =>$item): ?>
                                <div class="panel">
                                    <div class="panel-heading" role="tab" id="heading_<?= $args['widget_id'];?>_<?= $key; ?>">
                                        <h4 class="panel-title">
                                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_<?= $args['widget_id'];?>" href="#collapse_<?= $args['widget_id'];?>_<?= $key; ?>" aria-expanded="<?= $expanded; ?>" aria-controls="collapse_<?= $args['widget_id'];?>_<?= $key; ?>">
                                                <?= $item['title']; ?>
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse_<?= $args['widget_id'];?>_<?= $key; ?>" class="panel-collapse<?= $collapse; ?>" role="tabpanel" aria-labelledby="heading_<?= $args['widget_id'];?>_<?= $key; ?>">
                                        <div class="panel-body">
                                            <?= $item['desc']; ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if($expanded == 'true'){
                                    $expanded = 'false';
                                }
                                ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--end section-acсordion-->

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainInfoWidget");
});