<?php
class WinnbrainForm2Widget extends WP_Widget{
    public function __construct() {
        parent::__construct('Form-widget-2', __('Winnbrain Form 2 | Widget', 'winnbrain'),
           ['description' => __('Contact Form', 'winnbrain')]);
    }

    public function form($instance) {
        $title = '';
        $formTitle = '';
        $background = '';
        $sectionId = '';
        $status = '';

        if (!empty($instance)) {
            $title = esc_attr($instance['title']);
            $formTitle = esc_attr($instance['formTitle']);
            $background = esc_attr($instance['background']);
            $sectionId = esc_attr($instance['sectionId']);
            $status = esc_attr($instance['status']);
        }

        $Id = $this->get_field_id('title');
        $Name = $this->get_field_name('title');
        echo '<p><label for="' . $Id . '">' . __('Title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $title . '"></p>';

        $Id = $this->get_field_id('formTitle');
        $Name = $this->get_field_name('formTitle');
        echo '<p><label for="' . $Id . '">' . __('Form title', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $formTitle . '"></p>';

        $Id = $this->get_field_id('background');
        $Name = $this->get_field_name('background');
        echo '<p><label for="' . $Id . '">' . __('Background', 'winnbrain') . ': </label>';
        echo '<div class="widget-block-img">';
        if($background){
            $mediaId = get_attachment_file_id($background);
            $selectedMedia = '';
            if($mediaId){
                $selectedMedia = '<script>';
                $selectedMedia .= 'if(!selectedMedia["'.$Id.'"]){selectedMedia["'.$Id.'"] = [];}';
                $selectedMedia .= 'selectedMedia["'.$Id.'"].push('.$mediaId.');console.log(selectedMedia);';
                $selectedMedia .= '</script>';
            }
            add_thickbox();
            echo '<a class="thickbox" href="'.$background.'?TB_iframe=false&width=100%&height=100%"><img src="'.$background.'"></a>';
            echo $selectedMedia;
        }
        echo '</div>';
        echo '<input data-options="multiple:false,type:image,type:video" class="widget-upload-button button button-secondary" type="button" value="'.__('Choose', 'winnbrain').'" />';
        echo '<input class="input-value" id="'.$Id.'" type="hidden" name="'.$Name.'" value="'.$background.'" />';
        echo '<input class="widget-remove-button button button-secondary" type="button" value="'.__('Remove', 'winnbrain').'" />';
        echo '</p>';

        $Id = $this->get_field_id('sectionId');
        $Name = $this->get_field_name('sectionId');
        echo '<p><label for="' . $Id . '">' . __('Section Id', 'winnbrain') . ': </label>';
        echo '<input class="widefat" id="' . $Id . '" type="text" name="' . $Name . '" value="' . $sectionId . '"></p>';

        $Id = $this->get_field_id('status');
        $Name = $this->get_field_name('status');
        echo '<p><label for="' . $Id . '">' . __('Activate widget', 'winnbrain') . ': </label>';
        $checked = '';
        if($status == 1){
            $checked = ' checked';
        }
        echo '<input id="' . $Id . '" type="checkbox" name="' . $Name . '" value="1"'.$checked.'></p>';
    }

    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = $newInstance['title'];
        $values['formTitle'] = $newInstance['formTitle'];
        $values['background'] =  htmlentities($newInstance['background']);
        $values['sectionId'] = htmlentities($newInstance['sectionId']);
        $values['status'] = htmlentities($newInstance['status']);
        return $values;
    }

    public function widget($args, $instance) {

        if(!$instance['status']){
            return;
        }

        extract($args);
        $title = $instance['title'];
        $formTitle = esc_attr($instance['formTitle']);
        $background = $instance['background'];
        $sectionId = $instance['sectionId'];
        if($sectionId){
            $sectionId = ' id="'.$sectionId.'"';
        }

        if($background){
            $background = ' style="background-image: url('.$background.');"';
        }

        if(count($instance)>0) {

            ?>
            <section<?= $sectionId; ?> class="section-base section-form dark-section"<?= $background; ?>>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-wrap">
                                <?php if($title): ?>
                                <div class="form-title">
                                    <h2><?= $title; ?></h2>
                                </div>
                                <?php endif; ?>
                                <form data-url="<?= admin_url('admin-ajax.php'); ?>" method="post" class="form form-2">
                                    <input type="hidden" data-form="form-3" class="input-name hidden-data" value="<?= $formTitle; ?>">
                                    <div class="row">
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-name username" name="username" required placeholder="<?= __('Your name', 'winnbrain') ?>*">
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-company-name company" name="company" placeholder="<?= __('Company name', 'winnbrain') ?>">
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-phone phone" name="phone" required id="input-phone-2" placeholder="<?= __('Your phone', 'winnbrain') ?>*">
                                        </div>
                                        <div class="col-sm-6 col-xs-12">
                                            <input type="text" class="form-control input-email email" name="email" placeholder="<?= __('Your email', 'winnbrain') ?>">
                                        </div>
                                        <div class="col-xs-12">
                                            <textarea class="input-message message" name="message" rows="6" placeholder="<?= __('Message text', 'winnbrain') ?>"></textarea>
                                        </div>
                                        <div class="col-xs-12">
                                            <button type="submit" class="main-button white-button"><span><?= __('Submit', 'winnbrain') ?></span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php

        }

    }

}

add_action("widgets_init", function () {
    register_widget("WinnbrainForm2Widget");
});