<?php
global $text_domain;

?>
<!--begin footer-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-xs-12">
                <div class="logo-footer">
                    <a href="<?= getPolylangCurrentLang(true); ?>"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo-footer.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-3 col-lg-push-7 col-sm-10 col-xs-push-0 col-xs-12">
                <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('footer_soc_links')): ?>
                    <?php dynamic_sidebar('footer_soc_links') ?>
                <?php endif; ?>
            </div>
            <div class="col-lg-7 col-lg-pull-3 col-xs-12 col-xs-pull-0">
                <div class="footer-links-list">

                    <?php
                    $defaults = array(
                        'theme_location'  => 'bottom_menu',
                        'menu'            => '7',
                        'container'       => false,
                        'container_class' => '',
                        'container_id'    => '',
                        'menu_class'      => '',
                        'menu_id'         => '',
                        'echo'            => true,
                        'fallback_cb'     => '',
                        'before'          => '',
                        'after'           => '',
                        'link_before'     => '',
                        'link_after'      => '',
                        'items_wrap'      => '<ul>%3$s</ul>',
                        'depth'           => 0
                    );
                    wp_nav_menu($defaults);
                    ?>

                </div>
            </div>
            <div class="col-xs-12">
                <div class="divider"></div>
            </div>
            <div class="col-xs-12">
                <div class="copyright">
                    <?php if(function_exists('dynamic_sidebar') && is_active_sidebar('copyright')): ?>
                    <p><?php dynamic_sidebar('copyright') ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<!--begin modals-->
<!--begin modal slider-1-item-->
<div class="modal fade" id="modal-testimonials" tabindex="-1" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!--begin modal slider-1-item-->
<!--begin modal slider-1-item-->
<div class="modal fade" id="modal-1-item-slider" tabindex="-1" role="dialog">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<!--begin modal slider-1-item-->
<!--begin modal form-popup-->
<div class="modal fade" id="form-popup-1" tabindex="-1" role="dialog" aria-labelledby="form-popup-1">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-wrap">
                    <div class="form-header">
                        <h4><?= __('Submit your application', $text_domain); ?></h4>
                    </div>
                    <div class="form-description">
                        <?= __('Leave your contact information, an expert will contact you as soon as possible', $text_domain); ?>
                    </div>
                    <form data-url="<?= admin_url('admin-ajax.php'); ?>" method="post" class="form form-1">
                        <input type="hidden" data-form="form-2" class="input-name hidden-data" value="Оставить заявку">
                        <div class="row">
                            <div class="col-xs-12">
                                <input type="text" class="form-control input-rate" name="input-rate">
                            </div>
                            <div class="col-xs-12">
                                <input type="text" class="form-control input-name username" name="username" required placeholder="<?= __('Your name', $text_domain); ?>">
                            </div>
                            <div class="col-xs-12">
                                <input type="text" class="form-control input-phone phone" name="phone" required id="input-phone-3" placeholder="<?= __('Your phone', $text_domain); ?>">
                            </div>
                            <div class="col-xs-12">
                                <input type="text" class="form-control input-email email" name="email" placeholder="<?= __('Your email', $text_domain); ?>">
                            </div>
                            <div class="col-xs-12">
                                <button type="submit" class="main-button blue-button"><span><?= __('Submit', $text_domain); ?></span></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!--end modal form-popup-->
<!--end modals-->

<!--SCRIPTS-->
<?php wp_footer(); ?>

</body>
</html>