<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 *
 * Template Name: Blank Page
 * Template Post Type: page
 *
 * @package winnbrain
 * @since 1.0
 * @version 1.0
 */

get_header();

global $text_domain;

$parents = get_post_ancestors($post->ID);
if(is_array($parents) && count($parents)>0){
    $args = array(
        'post_type' => 'page',
        'post__in' => $parents
    );
    $parents = get_posts($args);
    $parents = count($parents)>0?array_reverse($parents):[];
}
?>

<?php while ( have_posts() ) : the_post(); ?>

    <!--begin section-breadcrumbs-->
    <section class="section-breadcrumbs">
        <div class="breadcrumbs-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="/"><?= __('Home', $text_domain); ?></a></li>
                            <?php
                            $content = get_the_content();
                            if(count($parents)){
                                foreach ($parents as $parent){
                                    echo '<li><a href="'.get_permalink($parent->ID).'">'.$parent->post_title.'</a></li>';
                                }
                            }
                            ?>
                            <li class="active"><?= get_the_title(); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end section-breadcrumbs-->

    <?php addSidebars($post->ID, 'top'); ?>

    <!--begin section-basic-content-->
    <section class="section-basic-content light-section"<?= preg_replace("/&#?[a-z0-9]+;/i","",strip_tags($content))?' style="margin-top: 80px;"':''; ?>>
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </section>
    <!--end section-basic-content-->

<?php endwhile; ?>

<?php addSidebars($post->ID, 'bottom'); ?>

<?php get_footer();
